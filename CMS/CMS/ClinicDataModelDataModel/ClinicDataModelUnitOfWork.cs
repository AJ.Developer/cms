﻿using CMS.DataLayer;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.EF6;
using System;

namespace CMS.ClinicDataModelDataModel
{
    public class ClinicDataModelUnitOfWork : DbUnitOfWork<ClinicDataModel>, IClinicDataModelUnitOfWork
    {

        public ClinicDataModelUnitOfWork(Func<ClinicDataModel> contextFactory)
            : base(contextFactory)
        {
        }
        IRepository<Expense, int> IClinicDataModelUnitOfWork.Expenses
        {
            get { return GetRepository(x => x.Set<Expense>(), (Expense x) => x.ExpenseID); }
        }
        IRepository<ExpenseItem, int> IClinicDataModelUnitOfWork.ExpenseItems
        {
            get { return GetRepository(x => x.Set<ExpenseItem>(), (ExpenseItem x) => x.ExpenseItemID); }
        }
        IRepository<ExpenseType, int> IClinicDataModelUnitOfWork.ExpenseTypes
        {
            get { return GetRepository(x => x.Set<ExpenseType>(), (ExpenseType x) => x.ExpenseTypeID); }
        }
        IRepository<Invoice, int> IClinicDataModelUnitOfWork.Invoices
        {
            get { return GetRepository(x => x.Set<Invoice>(), (Invoice x) => x.InvoiceID); }
        }
        IRepository<Treatment, int> IClinicDataModelUnitOfWork.Treatments
        {
            get { return GetRepository(x => x.Set<Treatment>(), (Treatment x) => x.TreatmentID); }
        }
        IRepository<Supplier, int> IClinicDataModelUnitOfWork.Suppliers
        {
            get { return GetRepository(x => x.Set<Supplier>(), (Supplier x) => x.SupplierID); }
        }
        IRepository<User, int> IClinicDataModelUnitOfWork.Users
        {
            get { return GetRepository(x => x.Set<User>(), (User x) => x.UserID); }
        }
        IRepository<DoctorGroup, int> IClinicDataModelUnitOfWork.DoctorGroups
        {
            get { return GetRepository(x => x.Set<DoctorGroup>(), (DoctorGroup x) => x.DoctorGroupID); }
        }
        IRepository<MedicationType, int> IClinicDataModelUnitOfWork.MedicationTypes
        {
            get { return GetRepository(x => x.Set<MedicationType>(), (MedicationType x) => x.MedicationTypeID); }
        }
        IRepository<Configuration, int> IClinicDataModelUnitOfWork.Configurations
        {
            get { return GetRepository(x => x.Set<Configuration>(), (Configuration x) => x.ID); }
        }
        IRepository<SaleItem, int> IClinicDataModelUnitOfWork.SaleItems
        {
            get { return GetRepository(x => x.Set<SaleItem>(), (SaleItem x) => x.SaleItemID); }
        }
        IRepository<SaleUnit, int> IClinicDataModelUnitOfWork.SaleUnits
        {
            get { return GetRepository(x => x.Set<SaleUnit>(), (SaleUnit x) => x.SaleUnitID); }
        }
        IRepository<StockItem, int> IClinicDataModelUnitOfWork.StockItems
        {
            get { return GetRepository(x => x.Set<StockItem>(), (StockItem x) => x.StockItemID); }
        }
        IRepository<InvoiceItem, int> IClinicDataModelUnitOfWork.InvoiceItems
        {
            get { return GetRepository(x => x.Set<InvoiceItem>(), (InvoiceItem x) => x.InvoiceItemID); }
        }

        IRepository<AppointmentLocation, int> IClinicDataModelUnitOfWork.AppointmentLocations
        {
            get { return GetRepository(x => x.Set<AppointmentLocation>(), (AppointmentLocation x) => x.AppointmentLocationID); }
        }

        IRepository<MedicalAppointment, int> IClinicDataModelUnitOfWork.MedicalAppointments
        {
            get { return GetRepository(x => x.Set<MedicalAppointment>(), (MedicalAppointment x) => x.MedicalAppointmentID); }
        }

        IRepository<Doctor, int> IClinicDataModelUnitOfWork.Doctors
        {
            get { return GetRepository(x => x.Set<Doctor>(), (Doctor x) => x.DoctorID); }
        }

        IRepository<HospitalDepartment, int> IClinicDataModelUnitOfWork.HospitalDepartments
        {
            get { return GetRepository(x => x.Set<HospitalDepartment>(), (HospitalDepartment x) => x.HospitalDepartmentID); }
        }

        IRepository<Patient, int> IClinicDataModelUnitOfWork.Patients
        {
            get { return GetRepository(x => x.Set<Patient>(), (Patient x) => x.PatientID); }
        }

        IRepository<PaymentState, int> IClinicDataModelUnitOfWork.PaymentStates
        {
            get { return GetRepository(x => x.Set<PaymentState>(), (PaymentState x) => x.PaymentStateID); }
        }

        IRepository<AppointmentImportance, int> IClinicDataModelUnitOfWork.AppointmentImportances
        {
            get { return GetRepository(x => x.Set<AppointmentImportance>(), (AppointmentImportance x) => x.AppointmentImportanceID); }
        }
    }
}
