﻿using CMS.DataLayer;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.DesignTime;

namespace CMS.ClinicDataModelDataModel
{

    public class ClinicDataModelDesignTimeUnitOfWork : DesignTimeUnitOfWork, IClinicDataModelUnitOfWork
    {

        public ClinicDataModelDesignTimeUnitOfWork()
        {
        }
        IRepository<Expense, int> IClinicDataModelUnitOfWork.Expenses
        {
            get { return GetRepository((Expense x) => x.ExpenseID); }
        }
        IRepository<ExpenseItem, int> IClinicDataModelUnitOfWork.ExpenseItems
        {
            get { return GetRepository((ExpenseItem x) => x.ExpenseItemID); }
        }
        IRepository<ExpenseType, int> IClinicDataModelUnitOfWork.ExpenseTypes
        {
            get { return GetRepository((ExpenseType x) => x.ExpenseTypeID); }
        }
        IRepository<Invoice, int> IClinicDataModelUnitOfWork.Invoices
        {
            get { return GetRepository((Invoice x) => x.InvoiceID); }
        }
        IRepository<Treatment, int> IClinicDataModelUnitOfWork.Treatments
        {
            get { return GetRepository((Treatment x) => x.TreatmentID); }
        }
        IRepository<Supplier, int> IClinicDataModelUnitOfWork.Suppliers
        {
            get { return GetRepository((Supplier x) => x.SupplierID); }
        }
        IRepository<DoctorGroup, int> IClinicDataModelUnitOfWork.DoctorGroups
        {
            get { return GetRepository((DoctorGroup x) => x.DoctorGroupID); }
        }
        IRepository<User, int> IClinicDataModelUnitOfWork.Users
        {
            get { return GetRepository((User x) => x.UserID); }
        }
        IRepository<MedicationType, int> IClinicDataModelUnitOfWork.MedicationTypes
        {
            get { return GetRepository((MedicationType x) => x.MedicationTypeID); }
        }
        IRepository<Configuration, int> IClinicDataModelUnitOfWork.Configurations
        {
            get { return GetRepository((Configuration x) => x.ID); }
        }
        IRepository<SaleItem, int> IClinicDataModelUnitOfWork.SaleItems
        {
            get { return GetRepository((SaleItem x) => x.SaleItemID); }
        }
        IRepository<SaleUnit, int> IClinicDataModelUnitOfWork.SaleUnits
        {
            get { return GetRepository((SaleUnit x) => x.SaleUnitID); }
        }
        IRepository<StockItem, int> IClinicDataModelUnitOfWork.StockItems
        {
            get { return GetRepository((StockItem x) => x.StockItemID); }
        }
        IRepository<InvoiceItem, int> IClinicDataModelUnitOfWork.InvoiceItems
        {
            get { return GetRepository((InvoiceItem x) => x.InvoiceItemID); }
        }
        IRepository<AppointmentLocation, int> IClinicDataModelUnitOfWork.AppointmentLocations
        {
            get { return GetRepository((AppointmentLocation x) => x.AppointmentLocationID); }
        }

        IRepository<MedicalAppointment, int> IClinicDataModelUnitOfWork.MedicalAppointments
        {
            get { return GetRepository((MedicalAppointment x) => x.MedicalAppointmentID); }
        }

        IRepository<Doctor, int> IClinicDataModelUnitOfWork.Doctors
        {
            get { return GetRepository((Doctor x) => x.DoctorID); }
        }

        IRepository<HospitalDepartment, int> IClinicDataModelUnitOfWork.HospitalDepartments
        {
            get { return GetRepository((HospitalDepartment x) => x.HospitalDepartmentID); }
        }

        IRepository<Patient, int> IClinicDataModelUnitOfWork.Patients
        {
            get { return GetRepository((Patient x) => x.PatientID); }
        }

        IRepository<PaymentState, int> IClinicDataModelUnitOfWork.PaymentStates
        {
            get { return GetRepository((PaymentState x) => x.PaymentStateID); }
        }
        IRepository<AppointmentImportance, int> IClinicDataModelUnitOfWork.AppointmentImportances
        {
            get { return GetRepository((AppointmentImportance x) => x.AppointmentImportanceID); }
        }
    }
}
