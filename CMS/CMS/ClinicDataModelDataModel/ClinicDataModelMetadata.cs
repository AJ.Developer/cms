﻿using CMS.DataLayer;
using CMS.Localization;
using DevExpress.Mvvm.DataAnnotations;

namespace CMS.ClinicDataModelDataModel
{
    public class ClinicDataModelMetadataProvider
    {
        public static void BuildMetadata(MetadataBuilder<AppointmentLocation> builder)
        {
            builder.DisplayName(ClinicDataModelResources.AppointmentLocation);
            builder.Property(x => x.AppointmentLocationID).DisplayName(ClinicDataModelResources.AppointmentLocation_AppointmentLocationID);
            builder.Property(x => x.Caption).DisplayName(ClinicDataModelResources.AppointmentLocation_Caption);
        }
        public static void BuildMetadata(MetadataBuilder<MedicalAppointment> builder)
        {
            builder.DisplayName(ClinicDataModelResources.MedicalAppointment);
            builder.Property(x => x.MedicalAppointmentID).DisplayName(ClinicDataModelResources.MedicalAppointment_MedicalAppointmentID);
            builder.Property(x => x.Type).DisplayName(ClinicDataModelResources.MedicalAppointment_Type);
            builder.Property(x => x.AllDay).DisplayName(ClinicDataModelResources.MedicalAppointment_AllDay);
            builder.Property(x => x.StartTime).DisplayName(ClinicDataModelResources.MedicalAppointment_StartTime);
            builder.Property(x => x.EndTime).DisplayName(ClinicDataModelResources.MedicalAppointment_EndTime);
            builder.Property(x => x.Subject).DisplayName(ClinicDataModelResources.MedicalAppointment_Subject);
            builder.Property(x => x.MedicationType).DisplayName(ClinicDataModelResources.MedicalAppointment_MedicationType);
            builder.Property(x => x.Note).DisplayName(ClinicDataModelResources.MedicalAppointment_Note);
            builder.Property(x => x.RecurrenceInfo).DisplayName(ClinicDataModelResources.MedicalAppointment_RecurrenceInfo);
            builder.Property(x => x.ReminderInfo).DisplayName(ClinicDataModelResources.MedicalAppointment_ReminderInfo);
            builder.Property(x => x.AppointmentLocation).DisplayName(ClinicDataModelResources.MedicalAppointment_AppointmentLocation);
            builder.Property(x => x.Doctor).DisplayName(ClinicDataModelResources.MedicalAppointment_Doctor);
            builder.Property(x => x.Patient).DisplayName(ClinicDataModelResources.MedicalAppointment_Patient);
            builder.Property(x => x.PaymentState).DisplayName(ClinicDataModelResources.MedicalAppointment_PaymentState);
        }
        public static void BuildMetadata(MetadataBuilder<Doctor> builder)
        {
            builder.DisplayName(ClinicDataModelResources.Doctor);
            builder.Property(x => x.DoctorID).DisplayName(ClinicDataModelResources.Doctor_DoctorID);
            builder.Property(x => x.Name).DisplayName(ClinicDataModelResources.Doctor_Name);
            builder.Property(x => x.Phone).DisplayName(ClinicDataModelResources.Doctor_Phone);
            builder.Property(x => x.IsVisible).DisplayName(ClinicDataModelResources.Doctor_IsVisible);
            builder.Property(x => x.HospitalDepartment).DisplayName(ClinicDataModelResources.Doctor_HospitalDepartment);
        }
        public static void BuildMetadata(MetadataBuilder<HospitalDepartment> builder)
        {
            builder.DisplayName(ClinicDataModelResources.HospitalDepartment);
            builder.Property(x => x.HospitalDepartmentID).DisplayName(ClinicDataModelResources.HospitalDepartment_HospitalDepartmentID);
            builder.Property(x => x.Name).DisplayName(ClinicDataModelResources.HospitalDepartment_Name);
        }
        public static void BuildMetadata(MetadataBuilder<Patient> builder)
        {
            builder.DisplayName(ClinicDataModelResources.Patient);
            builder.Property(x => x.PatientID).DisplayName(ClinicDataModelResources.Patient_PatientID);
            builder.Property(x => x.Name).DisplayName(ClinicDataModelResources.Patient_Name);
            builder.Property(x => x.BirthDate).DisplayName(ClinicDataModelResources.Patient_BirthDate);
            builder.Property(x => x.Phone).DisplayName(ClinicDataModelResources.Patient_Phone);
        }
        public static void BuildMetadata(MetadataBuilder<PaymentState> builder)
        {
            builder.DisplayName(ClinicDataModelResources.PaymentState);
            builder.Property(x => x.PaymentStateID).DisplayName(ClinicDataModelResources.PaymentState_PaymentStateID);
            builder.Property(x => x.Caption).DisplayName(ClinicDataModelResources.PaymentState_Caption);
            builder.Property(x => x.BrushName).DisplayName(ClinicDataModelResources.PaymentState_BrushName);
        }
    }
}