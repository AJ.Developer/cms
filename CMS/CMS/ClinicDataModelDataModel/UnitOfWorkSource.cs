﻿using CMS.DataLayer;
using CMS.Views;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.DesignTime;
using DevExpress.Mvvm.DataModel.EF6;

namespace CMS.ClinicDataModelDataModel
{
    public static class UnitOfWorkSource
    {
        public static IUnitOfWorkFactory<IClinicDataModelUnitOfWork> GetUnitOfWorkFactory()
        {
            return GetUnitOfWorkFactory(ViewModelBase.IsInDesignMode);
        }
        public static IUnitOfWorkFactory<IClinicDataModelUnitOfWork> GetUnitOfWorkFactory(bool isInDesignTime)
        {
            if (isInDesignTime)
                return new DesignTimeUnitOfWorkFactory<IClinicDataModelUnitOfWork>(() => new ClinicDataModelDesignTimeUnitOfWork());
            return new DbUnitOfWorkFactory<IClinicDataModelUnitOfWork>(() => new ClinicDataModelUnitOfWork(() => new ClinicDataModel()));
        }
        public static bool IsAdmin { get; set; }
        public static User CurrentUser { get; set; }
        public static LoginCollectionView LoginView { get; set; }
    }
}