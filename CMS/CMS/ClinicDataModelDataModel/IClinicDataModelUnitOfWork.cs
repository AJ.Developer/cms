﻿using CMS.DataLayer;
using DevExpress.Mvvm.DataModel;

namespace CMS.ClinicDataModelDataModel
{
    public interface IClinicDataModelUnitOfWork : IUnitOfWork
    {
        
        IRepository<Expense, int> Expenses { get; }
        IRepository<ExpenseItem, int> ExpenseItems { get; }
        IRepository<ExpenseType, int> ExpenseTypes { get; }
        IRepository<DoctorGroup, int> DoctorGroups { get; }
        IRepository<AppointmentLocation, int> AppointmentLocations { get; }
        IRepository<MedicationType, int> MedicationTypes { get; }
        IRepository<HospitalDepartment, int> HospitalDepartments { get; }
        IRepository<PaymentState, int> PaymentStates { get; }
        IRepository<AppointmentImportance, int> AppointmentImportances { get; }
        
        IRepository<Treatment, int> Treatments { get; }
        IRepository<Supplier, int> Suppliers { get; }
        IRepository<SaleUnit, int> SaleUnits { get; }
        IRepository<SaleItem, int> SaleItems { get; }
        IRepository<StockItem, int> StockItems { get; }
        IRepository<InvoiceItem, int> InvoiceItems { get; }
        IRepository<Invoice, int> Invoices { get; }
       
        IRepository<Doctor, int> Doctors { get; }
        IRepository<MedicalAppointment, int> MedicalAppointments { get; }
        IRepository<Patient, int> Patients { get; }
        IRepository<User, int> Users { get; }

        IRepository<Configuration, int> Configurations { get; }
    }
}
