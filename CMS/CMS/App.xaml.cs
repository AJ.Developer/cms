﻿using CMS.ClinicDataModelDataModel;
using CMS.Views;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Grid.LookUp;
using DevExpress.Xpf.LayoutControl;
using DevExpress.Xpf.WindowsUI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

namespace CMS
{
    public partial class App : Application
    {
        // Miliseconds 
        private const int MINIMUM_SPLASH_TIME = 3000;
        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                if (UnitOfWorkSource.LoginView == null)
                    UnitOfWorkSource.LoginView = new LoginCollectionView();
                UnitOfWorkSource.LoginView.Topmost = true;
                Stopwatch timer = new Stopwatch();
                timer.Start();
                base.OnStartup(e);
                LoadAssemblies();
                ShutdownMode = ShutdownMode.OnLastWindowClose;
                if (DateTime.Now.Year > 2021)
                    this.Shutdown();
                timer.Stop();
                int remainingTimeToShowSplash = MINIMUM_SPLASH_TIME - (int)timer.ElapsedMilliseconds;

                if (remainingTimeToShowSplash > 0)
                    Thread.Sleep(remainingTimeToShowSplash);
                UnitOfWorkSource.LoginView.Show();
                UnitOfWorkSource.LoginView.Activate();
                UnitOfWorkSource.LoginView.Topmost = false;
                base.OnStartup(e);
                ShutdownMode = ShutdownMode.OnLastWindowClose;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Current.ShutdownMode = ShutdownMode.OnLastWindowClose;
                Current.Shutdown(0);
            }

        }
        private void LoadAssemblies()
        {
            RunTypeInitializers(Assembly.GetAssembly(typeof(ButtonEdit)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(LayoutControl)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(LayoutItem)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(LayoutGroup)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(PageAdornerControl)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(ComboBoxEdit)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(LookUpEdit)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(GridControl)));

            RunTypeInitializers(Assembly.GetAssembly(typeof(TableView)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(ProgressBarEdit)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(SpinEdit)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(DateEdit)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(RadioButton)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(FlowLayoutControl)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(Button)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(GridColumn)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(GridControlBand)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(TextEditSettings)));
            RunTypeInitializers(Assembly.GetAssembly(typeof(GridSummaryItem)));
        }

        static void RunTypeInitializers(Assembly assembly)
        {
            Type[] types = assembly.GetExportedTypes();
            for (int i = 0; i < types.Length; i++)
            {
                RuntimeHelpers.RunClassConstructor(types[i].TypeHandle);
            }
        }
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                var comexception = e.Exception as System.Runtime.InteropServices.COMException;
                if (comexception != null && comexception.ErrorCode == -2147221040)
                    e.Handled = true;
                else if (e.Exception != null)
                    MessageBox.Show(e.Exception.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {

        }
    }
}
