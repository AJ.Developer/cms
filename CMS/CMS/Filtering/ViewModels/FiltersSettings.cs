
using CMS.ClinicDataModelDataModel;
using CMS.DataLayer;
using CMS.Properties;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.ViewModel;
using System;

namespace CMS.Filtering.ViewModels
{
    internal static class FiltersSettings
    {
        public static FilterTreeViewModel<Doctor, int> GetDoctorsFilterTree(object parentViewModel)
        {
            return FilterTreeViewModel<Doctor, int>.Create(
                new FilterTreeModelPageSpecificSettings<Settings>(Settings.Default, "Static", x => x.DoctorsStaticFilters, x => x.DoctorsCustomFilters,
                    new[] {
                        BindableBase.GetPropertyName(() => new Doctor().DoctorID),
                        BindableBase.GetPropertyName(() => new Doctor().IsVisible),
                        BindableBase.GetPropertyName(() => new Doctor().HospitalDepartment),
                        BindableBase.GetPropertyName(() => new Doctor().Photo),
                        BindableBase.GetPropertyName(() => new Doctor().HospitalDepartmentID),
                    },
                    new[] {
                        BindableBase.GetPropertyName(() => new Doctor().HospitalDepartment) + "." + BindableBase.GetPropertyName(() => new HospitalDepartment().Name),
                        //BindableBase.GetPropertyName(() => new Doctor().HospitalDepartment) + "." + BindableBase.GetPropertyName(() => new HospitalDepartment().HospitalDepartmentID),
                    }, "Custom"),
                CreateUnitOfWork().Doctors, new Action<object, Action>(RegisterEntityChangedMessageHandler<Doctor, int>)
            ).SetParentViewModel(parentViewModel);
        }
        public static FilterTreeViewModel<Expense, int> GetExpensesFilterTree(object parentViewModel)
        {
            return FilterTreeViewModel<Expense, int>.Create(
                new FilterTreeModelPageSpecificSettings<Settings>(Settings.Default, "Static", x => x.ExpensesStaticFilters, x => x.ExpensesCustomFilters,
                    new[] {
                        BindableBase.GetPropertyName(() => new Expense().ExpenseID),
                    },null, "Custom"),
                CreateUnitOfWork().Expenses, new Action<object, Action>(RegisterEntityChangedMessageHandler<Expense, int>)
            ).SetParentViewModel(parentViewModel);
        }
        public static FilterTreeViewModel<Patient, int> GetPatientsFilterTree(object parentViewModel)
        {
            return FilterTreeViewModel<Patient, int>.Create(
                new FilterTreeModelPageSpecificSettings<Settings>(Settings.Default, "Static", x => x.PatientsStaticFilters, x => x.PatientsCustomFilters,
                    new[] {
                                BindableBase.GetPropertyName(() => new Patient().PatientID),
                    },
                    null, "Custom"),
                CreateUnitOfWork().Patients, new Action<object, Action>(RegisterEntityChangedMessageHandler<Patient, int>)
            ).SetParentViewModel(parentViewModel);
        }
        public static FilterTreeViewModel<MedicalAppointment, int> GetAppointmentsFilterTree(object parentViewModel)
        {
            return FilterTreeViewModel<MedicalAppointment, int>.Create(
                new FilterTreeModelPageSpecificSettings<Settings>(Settings.Default, "Static", x => x.AppointmentsStaticFilters, x => x.AppointmentsCustomFilters,
                    new[] {
                        BindableBase.GetPropertyName(() => new MedicalAppointment().MedicalAppointmentID),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().DoctorID),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().Doctor),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().AppointmentLocationID),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().AppointmentLocation),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().AppointmentLocationCaption),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().Type),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().RecurrenceInfo),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().ReminderInfo),
                    },
                    new[] {
                        BindableBase.GetPropertyName(() => new MedicalAppointment().Doctor) + "." + BindableBase.GetPropertyName(() => new MedicalAppointment().Doctor.Name),
                        BindableBase.GetPropertyName(() => new MedicalAppointment().AppointmentLocation) + "." + BindableBase.GetPropertyName(() => new MedicalAppointment().AppointmentLocation.Caption),
                    }, "Custom"),
                CreateUnitOfWork().MedicalAppointments, new Action<object, Action>(RegisterEntityChangedMessageHandler<MedicalAppointment, int>)
            ).SetParentViewModel(parentViewModel);
        }
        public static FilterTreeViewModel<Invoice, int> GetInvoicesFilterTree(object parentViewModel)
        {
            return FilterTreeViewModel<Invoice, int>.Create(
                new FilterTreeModelPageSpecificSettings<Settings>(Settings.Default, "Static", x => x.InvoicesStaticFilters, x => x.InvoicesCustomFilters,
                    new[] {
                        BindableBase.GetPropertyName(() => new Invoice().InvoiceID),
                        BindableBase.GetPropertyName(() => new Invoice().InvoiceStatus),
                    },
                    new[] {
                        BindableBase.GetPropertyName(() => new Invoice().I_Creator) + "." + BindableBase.GetPropertyName(() => new Invoice().I_Creator.Username),
                        BindableBase.GetPropertyName(() => new Invoice().I_LastEditor) + "." + BindableBase.GetPropertyName(() => new Invoice().I_LastEditor.Username),
                    }, "Custom"),
                CreateUnitOfWork().Invoices, new Action<object, Action>(RegisterEntityChangedMessageHandler<Invoice, int>)
            ).SetParentViewModel(parentViewModel);
        }
        static IClinicDataModelUnitOfWork CreateUnitOfWork()
        {
            return UnitOfWorkSource.GetUnitOfWorkFactory().CreateUnitOfWork();
        }
        static void RegisterEntityChangedMessageHandler<TEntity, TPrimaryKey>(object recipient, Action handler)
        {
            Messenger.Default.Register<EntityMessage<TEntity, TPrimaryKey>>(recipient, message => handler());
        }
    }
}