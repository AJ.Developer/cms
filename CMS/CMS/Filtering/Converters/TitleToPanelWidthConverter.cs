﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace CMS.Filtering.Converters
{
    public class TitleToPanelWidthConverter : MarkupExtension, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values != null && (bool)values[0] && values[1].ToString() != "NaN" && (values[1]).ToString() == "200")
                return new GridLength(200);
            else
           if (values != null && !(bool)values[0] && values[1].ToString() != "NaN" && (values[1]).ToString() == "200")
                return new GridLength(30);
            else
                return new GridLength(0);
        }

        public object[] ConvertBack(
            object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
