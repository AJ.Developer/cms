﻿using CMS.Filtering.ViewModels;
using DevExpress.Xpf.Accordion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace CMS.Filtering.Converters
{
    public class TitleToWidthConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string && value.ToString() != "Reception Desk" && value.ToString() != "Accounting")
                return 200;
            else return 0;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is FilterCategory)
                return Binding.DoNothing;
            return value;
        }
    }
    
}
