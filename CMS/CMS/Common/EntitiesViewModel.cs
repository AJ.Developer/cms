﻿using System;
using System.Linq;
using DevExpress.Mvvm.ViewModel;
using DevExpress.Mvvm.DataModel;

namespace CMS.Common
{

    public abstract partial class EntitiesViewModel<TEntity, TProjection, TUnitOfWork> :
        EntitiesViewModelBase<TEntity, TProjection, TUnitOfWork>
        where TEntity : class
        where TProjection : class
        where TUnitOfWork : IUnitOfWork
    {
        protected EntitiesViewModel(
            IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory,
            Func<TUnitOfWork, IReadOnlyRepository<TEntity>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>> projection,
            UnitOfWorkPolicy unitOfWorkPolicy)
            : base(unitOfWorkFactory, getRepositoryFunc, projection, unitOfWorkPolicy)
        {
        }
    }
}