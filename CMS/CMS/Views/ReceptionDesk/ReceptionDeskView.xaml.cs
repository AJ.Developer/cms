﻿using CMS.ClinicDataModelDataModel;
using CMS.DataLayer;
using CMS.ViewModels;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Scheduling;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CMS.Views
{
    public partial class ReceptionDeskView : UserControl
    {
        public ReceptionDeskView()
        {
            InitializeComponent(); 
            DayView view = (DayView)this.scheduler.ActiveView;
            //view.TimeScales = new TimeSpanCollection { new TimeSpan(00, 5, 00), new TimeSpan(00,10,00) , new TimeSpan(00, 15, 00),  new TimeSpan(00, 30, 00) , new TimeSpan(1, 0, 00) };
            //view.TimeScale = new TimeSpan(0, 15, 0);
            view.TimeRulers[0].ShowMinutes = true;
            view.TimeRulers[0].AlwaysShowTimeDesignator = true;

        }
        void OnAppointmentWindowShowing(object sender, AppointmentWindowShowingEventArgs e)
        {
            e.Window.DataContext = MedicalAppointmentWindowViewModel.Create(e.Appointment, this.scheduler);
           
        }
        void OnDropAppointment(object sender, DropAppointmentEventArgs e)
        {
            e.Cancel = e.ConflictedAppointments.Where(x => x.Count > 0).FirstOrDefault() != null;
        }
        void OnStartAppointmentDragFromOutside(object sender, StartAppointmentDragFromOutsideEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(IEnumerable<Patient>)))
                ((IEnumerable<Patient>)e.Data.GetData(typeof(IEnumerable<Patient>))).ToList().ForEach(x => e.DragAppointments.Add(CreateAppointment(x)));
        }
        void OnStartRecordDrag(object sender, StartRecordDragEventArgs e)
        {
            e.Data.SetData(typeof(IEnumerable<Patient>), e.Records.Cast<Patient>());
            e.Handled = true;
        }
        AppointmentItem CreateAppointment(Patient patient)
        {
            var viewmodel = (ReceptionDeskViewModel)this.DataContext;
            AppointmentItem result = new AppointmentItem();
            result.CustomFields["PatientName"] = patient.Name;
            result.CustomFields["PatientID"] = patient.PatientID;

            result.CustomFields["CreatorID"] = UnitOfWorkSource.CurrentUser.UserID;
            result.CustomFields["CreationDate"] = DateTime.Now;

            result.CustomFields["LastEditorID"] = UnitOfWorkSource.CurrentUser.UserID;
            result.CustomFields["LastEditDate"] = DateTime.Now;

            result.CustomFields["AppointmentLocationCaption"] = viewmodel.Locations.FirstOrDefault().Caption;
            result.CustomFields["AppointmentLocationID"] = viewmodel.Locations.FirstOrDefault().AppointmentLocationID;

            result.StatusId = scheduler.StatusItems.FirstOrDefault().Id;
            result.LabelId = scheduler.LabelItems.FirstOrDefault().Id;

            result.Start = DateTime.Today;
            result.End = result.Start.AddMinutes(20);
            return result;
        }
        void OnCompleteRecordDragDrop(object sender, CompleteRecordDragDropEventArgs e)
        {
            e.Handled = true;
        }
        void OnDragRecordOver(object sender, DragRecordOverEventArgs e)
        {
            e.Effects = DragDropEffects.Move;
            e.Handled = true;
        }
        void OnDropRecord(object sender, DropRecordEventArgs e)
        {
            e.Handled = true;
        }

        private void scheduler_AppointmentEdited(object sender, AppointmentEditedEventArgs e)
        {

        }

    }
}
