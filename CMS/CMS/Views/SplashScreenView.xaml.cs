﻿using DevExpress.Xpf.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CMS.Views
{
    /// <summary>
    /// Interaction logic for LoadingSplashScreen.xaml
    /// </summary>
    public partial class SplashScreenView : Window, ISplashScreen
    {
        public SplashScreenView()
        {
            InitializeComponent();
        }
        void ISplashScreen.CloseSplashScreen()
        {
            this.Close();
        }
        void ISplashScreen.Progress(double value)
        {
            //progressBar.Value = value;
        }
        void ISplashScreen.SetProgressState(bool isIndeterminate)
        {
        }
    }
}
