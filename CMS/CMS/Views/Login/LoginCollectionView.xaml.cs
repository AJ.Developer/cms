﻿using System.Windows;
using System.Windows.Controls.Primitives;

namespace CMS.Views
{

    public partial class LoginCollectionView : Window
    {


        public LoginCollectionView()
        {
            InitializeComponent();
            pass.Focus();
        }
        private void BtnActionMinimize_OnClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        private void btnActionClose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void Thumb_OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            Left = Left + e.HorizontalChange;
            Top = Top + e.VerticalChange;
        }
    }
}
