﻿using CMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CMS.Views
{
    /// <summary>
    /// Interaction logic for InvoicesStatisticsCollectionView.xaml
    /// </summary>
    public partial class InvoicesStatisticsCollectionView : UserControl
    {
        public InvoicesStatisticsCollectionView()
        {
            InitializeComponent();
        }

        private void DashboardControl_DataLoading(object sender, DevExpress.DashboardCommon.DataLoadingEventArgs e)
        {
            InvoicesDashboard data = new InvoicesDashboard();
            data.Generate();
            e.Data = data.Data;
        }
    }
}
