﻿using CMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CMS.Views
{
    /// <summary>
    /// Interaction logic for ExpensesStatisticsCollectionView.xaml
    /// </summary>
    public partial class ExpensesStatisticsCollectionView : UserControl
    {
        public ExpensesStatisticsCollectionView()
        {
            InitializeComponent();
        }

        private void DashboardControl_DataLoading(object sender, DevExpress.DashboardCommon.DataLoadingEventArgs e)
        {
            ExpensesDashboard data = new ExpensesDashboard();
            data.Generate();
            e.Data = data.Data;
        }
    }
}
