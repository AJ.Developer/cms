﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("ExpenseItems")]
    public class ExpenseItem
    {
        [Key]
        public int ExpenseItemID { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        //ExpenseType
        public int ExpenseTypeID { get; set; }
        [ForeignKey("ExpenseTypeID")]
        public virtual ExpenseType ExpenseType { get; set; }

        //Expense
        public int ExpenseID { get; set; }
        [ForeignKey("ExpenseID")]
        public virtual Expense Expense { get; set; }
    }
}
