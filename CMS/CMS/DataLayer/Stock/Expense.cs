﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("Expenses")]
    public class Expense : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "ExpenseNumber":
                        if (string.IsNullOrWhiteSpace(this.ExpenseNumber))
                            return "Expense Number Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }
        [Key]
        public int ExpenseID { get; set; }
        public string ExpenseNumber { get; set; }

        public decimal PaidAmount { get; set; }
        [NotMapped]
        public decimal TotalAmount
        {
            get
            {
                if (this.ExpenseItems != null)
                {
                    decimal amount = 0;
                    foreach (ExpenseItem item in ExpenseItems)
                        amount += ( item.Amount);
                    return amount;
                }
                else
                    return 0;
            }
        }
        [NotMapped]
        public decimal RestAmount
        {
            get
            {
                return this.TotalAmount - this.PaidAmount;
            }
        }

        public ExpenseStatus ExpenseStatus { get; set; }
        //Creator
        public int CreatorID { get; set; }
        [ForeignKey("CreatorID")]
        public virtual User E_Creator { get; set; }
        public DateTime? CreationDate { get; set; }

        //LastEditor
        public int LastEditorID { get; set; }
        [ForeignKey("LastEditorID")]
        public virtual User E_LastEditor { get; set; }
        public DateTime? LastEditDate { get; set; }
        public virtual string Notes { get; set; }
        public virtual ICollection<ExpenseItem> ExpenseItems { get; set; }

        [NotMapped]
        public string Error => "";
    }
    public enum ExpenseStatus
    {
        Draft,
        Done,
        Canceled
    }
}
