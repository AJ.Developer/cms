﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("Invoices")]
    public class Invoice : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {

                    case "SupplierID":
                        if (this.SupplierID == null)
                            return "Supplier Is Required";
                        break;
                    case "InvoiceNumber":
                        if (string.IsNullOrWhiteSpace(this.InvoiceNumber))
                            return "Invoice Number Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }
        [Key]
        public int InvoiceID { get; set; }
        public string InvoiceNumber { get; set; }

        public decimal PaidAmount { get; set; }
        [NotMapped]
        public decimal TotalAmount
        {
            get
            {
                if (this.InvoiceItems != null)
                {
                    decimal amount = 0;
                    foreach (InvoiceItem item in InvoiceItems)
                        amount += (item.Quantity * item.Price);
                    return amount;
                }
                else
                    return 0;
            }
        }
        [NotMapped]
        public decimal RestAmount
        {
            get
            {
                return this.TotalAmount - this.PaidAmount;
            }
        }

        public InvoiceStatus InvoiceStatus { get; set; }
        public string InvoiceStatusAsString
        {
            get
            {
                if (InvoiceStatus == InvoiceStatus.Draft)
                    return "Draft";
                if (InvoiceStatus == InvoiceStatus.Ordered)
                    return "Ordered";
                if (InvoiceStatus == InvoiceStatus.Done)
                    return "Done";
                if (InvoiceStatus == InvoiceStatus.Canceled)
                    return "Canceled";
                return "";
            }

        }

        //Supplier
        public int? SupplierID { get; set; }
        [ForeignKey("SupplierID")]
        public virtual Supplier Supplier { get; set; }

        //Creator
        public int CreatorID { get; set; }
        [ForeignKey("CreatorID")]
        public virtual User I_Creator { get; set; }
        public DateTime? CreationDate { get; set; }

        //LastEditor
        public int LastEditorID { get; set; }
        [ForeignKey("LastEditorID")]
        public virtual User I_LastEditor { get; set; }
        public DateTime? LastEditDate { get; set; }

        //Dates
        public DateTime? OrderingDate { get; set; }
        public DateTime? DeliveryDate { get; set; }

        public virtual string Notes { get; set; }
        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        [NotMapped]
        public string Error => "";
    }
    public enum InvoiceStatus
    {
        Draft,
        Ordered,
        Done,
        Canceled
    }
}
