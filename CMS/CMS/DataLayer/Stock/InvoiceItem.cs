﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("InvoiceItems")]
    public class InvoiceItem
    {
        [Key]
        public int InvoiceItemID { get; set; }
        [Required]
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        [NotMapped]
        public decimal TotalPrice
        { get { return Quantity * Price; } }

        //StockItem
        public int StockItemID { get; set; }
        [ForeignKey("StockItemID")]
        public virtual StockItem StockItem { get; set; }

        //Invoice
        public int InvoiceID { get; set; }
        [ForeignKey("InvoiceID")]
        public virtual Invoice Invoice { get; set; }
    }
}
