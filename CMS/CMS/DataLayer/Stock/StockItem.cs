﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("StockItems")]
    public class StockItem : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Name":
                        if (string.IsNullOrEmpty(this.Name))
                            return "Name Is Required";
                        break;
                    case "PurchasePrice":
                        if (this.PurchasePrice < 0)
                            return "Minimum Purchase Price Is 0 LBP";
                        break;
                    case "SalePrice":
                        if (this.SalePrice < 0)
                            return "Minimum Sale Price Is 0 LBP";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }

        [Key]
        public int StockItemID { get; set; }
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public decimal PurchasePrice { get; set; }
        [Required]
        public decimal SalePrice { get; set; }
        public bool IsConsumable { get; set; }
        [Required]
        public int Quantity { get; set; }
        public virtual ICollection<SaleUnit> SaleUnits { get; set; }
        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
