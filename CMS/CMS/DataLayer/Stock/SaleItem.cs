﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("SaleItems")]
    public class SaleItem : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Price":
                        if (this.Price < 0)
                            return "Minimum Price Is 250 LBP";
                        break;
                    case "Name":
                        if (string.IsNullOrEmpty(this.Name))
                            return "Name Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }

        [Key]
        public int SaleItemID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }
        public bool IsAvailable { get; set; }

        public virtual ICollection<SaleUnit> SaleUnits { get; set; }
        public virtual ICollection<Treatment> Treatments { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
