﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("Treatments")]
    public class Treatment
    {
        [Key]
        public int TreatmentID { get; set; }
        [Required]
        public int Quantity { get; set; }
        public int MyQuantity { get; set; }
        public decimal Price { get; set; }
        [NotMapped]
        public decimal TotalPrice
        { get { return Quantity * Price; } }
        //SaleItem
        public int SaleItemID { get; set; }
        [ForeignKey("SaleItemID")]
        public virtual SaleItem SaleItem { get; set; }

        //MedicalAppointment
        public int MedicalAppointmentID { get; set; }
        [ForeignKey("MedicalAppointmentID")]
        public virtual MedicalAppointment MedicalAppointment { get; set; }
    }
}
