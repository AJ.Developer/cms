﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("ExpenseTypes")]
    public class ExpenseType : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Description":
                        if (string.IsNullOrEmpty(this.Description))
                            return "Description Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }

        [Key]
        public int ExpenseTypeID { get; set; }
        [Required]
        public virtual string Description { get; set; }       
        public virtual ICollection<ExpenseItem> ExpenseItems { get; set; }
        [NotMapped]
        public string Error => "";
    }
}
