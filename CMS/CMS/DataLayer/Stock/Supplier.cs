using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("Suppliers")]
    public partial class Supplier : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Name":
                        if (string.IsNullOrWhiteSpace(this.Name))
                            return "Name Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }
        [Key]
        public int SupplierID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Address { get; set; }
        public virtual string Notes { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
        
        [NotMapped]
        public string Error => "";
    }
}
