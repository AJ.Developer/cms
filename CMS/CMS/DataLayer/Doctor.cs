using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("Doctors")]
    public partial class Doctor : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "HospitalDepartmentID":
                        if (this.HospitalDepartmentID < 1)
                            return "Hospital Department Is Required";
                        break;
                    case "DoctorGroupID":
                        if (this.DoctorGroupID < 1)
                            return "Doctor Group Is Required";
                        break;
                    case "Name":
                        if (string.IsNullOrWhiteSpace(this.Name))
                            return "Name Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }
        [Key]
        public int DoctorID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Phone { get; set; }
        public virtual object Photo { get; set; }
        [NotMapped]
        public virtual string Group
        {
            get
            {
                if (DoctorGroup != null)
                    return DoctorGroup.Name;
                else
                    return null;
            }
        }
        public virtual bool OnCallDay { get; set; }
        public virtual bool OnCallNight { get; set; }

        //HospitalDepartment
        public int HospitalDepartmentID { get; set; }
        [ForeignKey("HospitalDepartmentID")]
        public virtual HospitalDepartment HospitalDepartment { get; set; }

        //DoctorGroup
        public int DoctorGroupID { get; set; }
        [ForeignKey("DoctorGroupID")]
        public virtual DoctorGroup DoctorGroup { get; set; }
       
        public virtual bool IsVisible { get; set; }
        public virtual ICollection<MedicalAppointment> MedicalAppointments { get; set; }
        
        [NotMapped]
        public string Error => "";
    }
}
