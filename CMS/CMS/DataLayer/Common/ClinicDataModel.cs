using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CMS.DataLayer
{
    public partial class ClinicDataModel : DbContext
    {
        public ClinicDataModel()
            : base("name=ClinicDataModel")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ClinicDataModel, CMS.Migrations.Configuration>());
        }
        public virtual DbSet<Expense> Expenses { get; set; }
        public virtual DbSet<ExpenseItem> ExpenseItems { get; set; }
        public virtual DbSet<ExpenseType> ExpenseTypes { get; set; }
        public virtual DbSet<AppointmentLocation> AppointmentLocations { get; set; }
        public virtual DbSet<Doctor> Doctors { get; set; }
        public virtual DbSet<HospitalDepartment> HospitalDepartments { get; set; }
        public virtual DbSet<MedicalAppointment> MedicalAppointments { get; set; }
        public virtual DbSet<Patient> Patients { get; set; }
        public virtual DbSet<PaymentState> PaymentStates { get; set; }
        public virtual DbSet<AppointmentImportance> AppointmentImportances { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<SaleItem> SaleItems { get; set; }
        public virtual DbSet<StockItem> StockItems { get; set; }
        public virtual DbSet<SaleUnit> SaleUnits { get; set; }
        public virtual DbSet<InvoiceItem> InvoiceItems { get; set; }
        public virtual DbSet<MedicationType> MedicationTypes { get; set; }
        public virtual DbSet<Configuration> Configurations { get; set; }
        public virtual DbSet<DoctorGroup> DoctorGroups { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Treatment> Treatments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
