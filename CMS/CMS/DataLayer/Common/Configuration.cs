﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CMS
{
    public class Configuration
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
