using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("DoctorGroups")]
    public partial class DoctorGroup : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Name") && string.IsNullOrEmpty(this.Name))
                    return "Name Is Required";
                return string.Empty;
            }
        }
        [Key]
        public int DoctorGroupID { get; set; }
        public virtual string Name { get; set; }
        public ICollection<Doctor> Doctors { get; private set; }
        [NotMapped]
        public string Error => "";
    }
}
