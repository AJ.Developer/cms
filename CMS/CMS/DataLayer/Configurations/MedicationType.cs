﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.DataLayer
{
    [Table("MedicationTypes")]
    public class MedicationType : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Description") && string.IsNullOrEmpty(this.Description))
                        return "Description Is Required";
                return string.Empty;
            }
        }
        [Key]
        public int MedicationTypeID { get; set; }
        [Required]
        public string Description { get; set; }
        [NotMapped]
        public string Error => "";
    }
}
