using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("AppointmentLocations")]
    public partial class AppointmentLocation : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Caption") && string.IsNullOrEmpty(this.Caption))
                    return "Caption Is Required";
                return string.Empty;
            }
        }
        [Key]
        public int AppointmentLocationID { get; set; }
        public virtual string Caption { get; set; }
        public ICollection<MedicalAppointment> MedicalAppointments { get; set; }
        [NotMapped]
        public string Error => "";
    }
}
