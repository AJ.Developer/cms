using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("PaymentStates")]
    public partial class PaymentState : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Caption") && string.IsNullOrEmpty(this.Caption))
                    return "Caption Is Required";
                return string.Empty;
            }
        }
        [Key]
        public int PaymentStateID { get; set; }
        public virtual string Caption { get; set; }
        public virtual string Brush { get; set; }
        public virtual bool IsDone { get; set; }
        public virtual string BrushName
        {
            get
            {
                if (Brush != null)
                {
                    //Transparent
                    if (Brush.Equals("Transparent"))
                        return "StatusFree";
                    //Blue Mna22at
                    if (Brush.Equals("Dotted Blue"))
                        return "StatusWorkingElsewhere";
                    //Blue m5attat
                    if (Brush.Equals("Stripped Blue"))
                        return "StatusTentative";
                    //Red
                    if (Brush.Equals("Red"))
                        return "StatusOutOfOffice";
                    //Blue 
                    if (Brush.Equals("Blue"))
                        return "StatusBusy";
                }
                return "StatusFree";
            }

        }
        public ICollection<MedicalAppointment> MedicalAppointments { get; set; }
        [NotMapped]
        public string Error => "";
    }
}
