using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("AppointmentImportances")]
    public partial class AppointmentImportance : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Caption") && string.IsNullOrEmpty(this.Caption))
                    return "Caption Is Required";
                return string.Empty;
            }
        }
        [Key]
        public int AppointmentImportanceID { get; set; }
        public virtual string Caption { get; set; }
        public virtual string Brush { get; set; }
        public virtual string BrushName 
        {
            get
            {
                if (Brush != null)
                {
                    if (Brush.Equals("Transparent"))
                        return "LabelNone";
                    if (Brush.Equals("Red"))
                        return "LabelImportant";
                    if (Brush.Equals("Olive"))
                        return "LabelNeedsPreparation"; 
                    if (Brush.Equals("Green"))
                        return "LabelPersonal";
                    if (Brush.Equals("Orange")) 
                        return "LabelMustAttend";
                }
                return "LabelNone";
            }
           
        }
        public ICollection<MedicalAppointment> MedicalAppointments { get; set; }
        [NotMapped]
        public string Error => "";
    }
}
