﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.DataLayer
{
    [Table("Users")]
    public class User : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Username") && string.IsNullOrEmpty(this.Username))
                    return "Username Is Required";
                return string.Empty;
            }
        }

        [Key]
        public int UserID { get; set; }

        [Required]
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }

        [InverseProperty("I_Creator")]
        public virtual ICollection<Invoice> I_CreatedBy { get; set; }
        [InverseProperty("I_LastEditor")]
        public virtual ICollection<Invoice> I_LastEditedBy { get; set; }

        [InverseProperty("A_Creator")]
        public virtual ICollection<MedicalAppointment> A_CreatedBy { get; set; }
        [InverseProperty("A_LastEditor")]
        public virtual ICollection<MedicalAppointment> A_LastEditedBy { get; set; }
        [InverseProperty("E_Creator")]
        public virtual ICollection<Expense> E_CreatedBy { get; set; }
        [InverseProperty("E_LastEditor")]
        public virtual ICollection<Expense> E_LastEditedBy { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
