using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.DataLayer
{
    [Table("MedicalAppointments")]
    public partial class MedicalAppointment : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "StartTime":
                        if (this.StartTime == null)
                            return "Start Time Is Required";
                        break;
                    case "EndTime":
                        if (this.EndTime == null)
                            return "End Time Is Required";
                        break;
                    case "DoctorID":
                        if (this.DoctorID == null)
                            return "Doctor Is Required";
                        break;
                    case "PatientID":
                        if (this.PatientID == null)
                            return "Patient Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }
        public MedicalAppointment()
        {
            Treatments = new HashSet<Treatment>();
        }
        [Key]
        public int MedicalAppointmentID { get; set; }
        
        //Patient
        public virtual int? PatientID { get; set; }
        [ForeignKey("PatientID")]
        public virtual Patient Patient { get; set; }
        public virtual string PatientName { get; set; }
       
        //Doctor
        public virtual int? DoctorID { get; set; }
        [ForeignKey("DoctorID")]
        public virtual Doctor Doctor { get; set; }
       
        //AppointmentLocation
        public virtual int? AppointmentLocationID { get; set; }
        [ForeignKey("AppointmentLocationID")]
        public virtual AppointmentLocation AppointmentLocation { get; set; }
        public virtual string AppointmentLocationCaption { get; set; }

        //PaymentState
        public virtual int? PaymentStateID { get; set; }
        [ForeignKey("PaymentStateID")]
        public virtual PaymentState PaymentState { get; set; }
       
        //Creator
        public int CreatorID { get; set; }
        [ForeignKey("CreatorID")]
        public virtual User A_Creator { get; set; }
        public DateTime? CreationDate { get; set; }

        //LastEditor
        public int LastEditorID { get; set; }
        [ForeignKey("LastEditorID")]
        public virtual User A_LastEditor { get; set; }
        public DateTime? LastEditDate { get; set; }

        //AppointmentImportance
        public virtual int? AppointmentImportanceID { get; set; }
        [ForeignKey("AppointmentImportanceID")]
        public virtual AppointmentImportance AppointmentImportance { get; set; }
       
        //MedicationType
        public virtual int? MedicationTypeID { get; set; }
        [ForeignKey("MedicationTypeID")]
        public virtual MedicationType MedicationType { get; set; }
        public virtual string MedicationTypeDescription { get; set; }
        
        public virtual string Number { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Note { get; set; }
        public virtual string RecurrenceInfo { get; set; }
        public virtual string ReminderInfo { get; set; }
        public virtual int Type { get; set; }
        public virtual bool AllDay { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime EndTime { get; set; }
        
        public virtual ICollection<Treatment> Treatments { get; set; }
        [NotMapped]
        public decimal TotalAmount
        {
            get
            {
                if (this.Treatments != null)
                {
                    decimal amount = 0;
                    foreach (Treatment item in Treatments)
                        amount += (item.Quantity * item.Price);
                    return amount;
                }
                else
                    return 0;
            }
        }
        [NotMapped]
        public string Error => "";
    }
}
