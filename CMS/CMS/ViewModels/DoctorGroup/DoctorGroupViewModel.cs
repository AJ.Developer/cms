﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class DoctorGroupViewModel : SingleObjectViewModel<DoctorGroup, int, IClinicDataModelUnitOfWork>
    {

        public static DoctorGroupViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new DoctorGroupViewModel(unitOfWorkFactory));
        }

        protected DoctorGroupViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.DoctorGroups, x => x.DoctorGroupID)
        {
        }

        public IEntitiesViewModel<Doctor> LookUpDoctors
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (DoctorGroupViewModel x) => x.LookUpDoctors,
                    getRepositoryFunc: x => x.Doctors);
            }
        }

        public CollectionViewModelBase<Doctor, Doctor, int, IClinicDataModelUnitOfWork> DoctorGroupDoctorsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (DoctorGroupViewModel x) => x.DoctorGroupDoctorsDetails,
                    getRepositoryFunc: x => x.Doctors,
                    foreignKeyExpression: x => x.DoctorGroupID,
                    navigationExpression: x => x.DoctorGroup);
            }
        }
    }
}
