﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;

namespace CMS.ViewModels
{
    public partial class DoctorGroupCollectionViewModel : CollectionViewModel<DoctorGroup, int, IClinicDataModelUnitOfWork>, ISupportFiltering<DoctorGroup>
    {
        public static DoctorGroupCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new DoctorGroupCollectionViewModel(unitOfWorkFactory));
        }

        protected DoctorGroupCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.DoctorGroups)
        {
        }
        Expression<Func<DoctorGroup, bool>> ISupportFiltering<DoctorGroup>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<DoctorGroup>());
        }
    }
}