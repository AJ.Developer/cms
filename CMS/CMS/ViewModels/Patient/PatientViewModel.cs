﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class PatientViewModel : SingleObjectViewModel<Patient, int, IClinicDataModelUnitOfWork>
    {

        public static PatientViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new PatientViewModel(unitOfWorkFactory));
        }

        protected PatientViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Patients, x => x.PatientID)
        {
        }

        public IEntitiesViewModel<MedicalAppointment> LookUpMedicalAppointments
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (PatientViewModel x) => x.LookUpMedicalAppointments,
                    getRepositoryFunc: x => x.MedicalAppointments);
            }
        }

        public CollectionViewModelBase<MedicalAppointment, MedicalAppointment, int, IClinicDataModelUnitOfWork> PatientMedicalAppointmentsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (PatientViewModel x) => x.PatientMedicalAppointmentsDetails,
                    getRepositoryFunc: x => x.MedicalAppointments,
                    foreignKeyExpression: x => x.PatientID,
                    navigationExpression: x => x.Patient);
            }
        }
    }
}
