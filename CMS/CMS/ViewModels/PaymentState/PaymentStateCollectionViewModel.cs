﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using CMS.Filtering.ViewModels;
using System.Linq.Expressions;

namespace CMS.ViewModels
{
    public partial class PaymentStateCollectionViewModel : CollectionViewModel<PaymentState, int, IClinicDataModelUnitOfWork>, ISupportFiltering<PaymentState>
    {
        public static PaymentStateCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new PaymentStateCollectionViewModel(unitOfWorkFactory));
        }

        protected PaymentStateCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.PaymentStates)
        {
        }
        Expression<Func<PaymentState, bool>> ISupportFiltering<PaymentState>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<PaymentState>());
        }
    }
}