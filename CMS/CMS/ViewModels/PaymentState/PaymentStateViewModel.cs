﻿using System.Collections.Generic;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class PaymentStateViewModel : SingleObjectViewModel<PaymentState, int, IClinicDataModelUnitOfWork>
    {

        public static PaymentStateViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new PaymentStateViewModel(unitOfWorkFactory));
        }
        protected PaymentStateViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.PaymentStates, x => x.PaymentStateID)
        {
        }

        public List<string> StatusBrushs { get; set; } = new List<string> { "Transparent", "Dotted Blue", "Stripped Blue", "Red", "Blue" };

        public IEntitiesViewModel<MedicalAppointment> LookUpMedicalAppointments
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (PaymentStateViewModel x) => x.LookUpMedicalAppointments,
                    getRepositoryFunc: x => x.MedicalAppointments);
            }
        }

        public CollectionViewModelBase<MedicalAppointment, MedicalAppointment, int, IClinicDataModelUnitOfWork> PaymentStateMedicalAppointmentsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (PaymentStateViewModel x) => x.PaymentStateMedicalAppointmentsDetails,
                    getRepositoryFunc: x => x.MedicalAppointments,
                    foreignKeyExpression: x => x.PaymentStateID,
                    navigationExpression: x => x.PaymentState);
            }
        }
    }
}
