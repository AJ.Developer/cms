﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using CMS.Filtering.ViewModels;
using System.Linq.Expressions;
using DevExpress.Mvvm;

namespace CMS.ViewModels
{
    public partial class MedicationTypeCollectionViewModel : CollectionViewModel<MedicationType, int, IClinicDataModelUnitOfWork>, ISupportFiltering<MedicationType>
    {

        public static MedicationTypeCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new MedicationTypeCollectionViewModel(unitOfWorkFactory));
        }

        protected MedicationTypeCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.MedicationTypes)
        {
        }
        Expression<Func<MedicationType, bool>> ISupportFiltering<MedicationType>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<MedicationType>());
        }
    }
}