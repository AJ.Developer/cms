﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{
    public partial class MedicationTypeViewModel : SingleObjectViewModel<MedicationType, int, IClinicDataModelUnitOfWork>
    {
        public static MedicationTypeViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new MedicationTypeViewModel(unitOfWorkFactory));
        }

        protected MedicationTypeViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.MedicationTypes, x => x.MedicationTypeID)
        {
        }
        public IEntitiesViewModel<MedicalAppointment> LookUpMedicalAppointments
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (MedicationTypeViewModel x) => x.LookUpMedicalAppointments,
                    getRepositoryFunc: x => x.MedicalAppointments);
            }
        }
        public CollectionViewModelBase<MedicalAppointment, MedicalAppointment, int, IClinicDataModelUnitOfWork> MedicationTypeMedicalAppointmentsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (MedicationTypeViewModel x) => x.MedicationTypeMedicalAppointmentsDetails,
                    getRepositoryFunc: x => x.MedicalAppointments,
                    foreignKeyExpression: x => x.MedicationTypeID,
                    navigationExpression: x => x.MedicationType);
            }
        }
    }
}
