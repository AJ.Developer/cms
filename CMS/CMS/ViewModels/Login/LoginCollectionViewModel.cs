﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using System.Linq;
using DevExpress.Mvvm;
using System.Windows;
using CMS.Common;
using CMS.DataLayer;
using CMS.ClinicDataModelDataModel;

namespace CMS.ViewModels
{

    public partial class LoginCollectionViewModel : CollectionViewModel<User, int, IClinicDataModelUnitOfWork>
    {
        public static LoginCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new LoginCollectionViewModel(unitOfWorkFactory));
        }

        User LastUser;
        protected LoginCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Users)
        {
            LoadEntities(true);
            using (var context = new ClinicDataModel())
            {
                Configuration configuration = context.Configurations.Where(x => x.Key == "LastUser").FirstOrDefault();
                if (configuration != null)
                {
                    LastUser = context.Users.Where(x => x.Username == configuration.Value).FirstOrDefault();
                    if (LastUser != null)
                    {
                        UserName = LastUser.Username;
                        this.RaisePropertiesChanged();
                    }

                }
            }
        }

        public string UserName { get; set; }
        public string PassWord { get; set; }
        private void CloseAllWindowsAndOpenMainWindow()
        {
            MainWindow MainWindow = new MainWindow();
            Application.Current.MainWindow = MainWindow;
            WindowCollection col = App.Current.Windows;
            foreach (Window w in col)
            {
                if (w.ToString() == "CMS.Views.LoginCollectionView")
                    w.Close();
            }

            UnitOfWorkSource.LoginView = null;
            MainWindow.Show();
        }
        public void Login()
        {
            if (UserName == "AAA" && PassWord == "1982gonz")
            {

                UnitOfWorkSource.IsAdmin = true;
                CloseAllWindowsAndOpenMainWindow();
                return;
            }
            if (!string.IsNullOrEmpty(UserName))
            {
                User user = this.Entities.Where(x => x.Username == UserName && x.Password == PassWord).FirstOrDefault();
                if (user != null)
                {

                    UnitOfWorkSource.IsAdmin = user.IsAdmin;
                    UnitOfWorkSource.CurrentUser = user;
                    using (var context = new ClinicDataModel())
                    {
                        if (context.Configurations.ToList().Where(x => x.Key == "LastUser").ToList().Count == 0)
                            context.Configurations.Add(new Configuration() { Key = "LastUser", Value = UserName });
                        else
                            context.Configurations.ToList().Where(x => x.Key == "LastUser").FirstOrDefault().Value = UserName;
                        context.SaveChanges();
                    }
                    CloseAllWindowsAndOpenMainWindow();
                }
                else
                {
                    PassWord = "";
                    MessageBoxService.ShowMessage("Wrong Username or Password", "Failed", MessageButton.OK, MessageIcon.Stop);
                    this.RaisePropertiesChanged();
                }
            }
            else
            {
                MessageBoxService.ShowMessage("Username Is Required", "Failed", MessageButton.OK, MessageIcon.Warning);
            }

        }


    }
}

