﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using CMS.Filtering.ViewModels;
using System.Linq.Expressions;
using DevExpress.Mvvm;

namespace CMS.ViewModels
{

    public partial class DoctorCollectionViewModel : CollectionViewModel<Doctor, int, IClinicDataModelUnitOfWork>, ISupportFiltering<Doctor>
    {
        public static DoctorCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new DoctorCollectionViewModel(unitOfWorkFactory));
        }

        protected DoctorCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Doctors)
        {
        }
        Expression<Func<Doctor, bool>> ISupportFiltering<Doctor>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<Doctor>());
        }
    }
}