﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class DoctorViewModel : SingleObjectViewModel<Doctor, int, IClinicDataModelUnitOfWork>
    {

        public static DoctorViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new DoctorViewModel(unitOfWorkFactory));
        }

        protected DoctorViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Doctors, x => x.DoctorID)
        {
        }

        public IEntitiesViewModel<MedicalAppointment> LookUpMedicalAppointments
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (DoctorViewModel x) => x.LookUpMedicalAppointments,
                    getRepositoryFunc: x => x.MedicalAppointments);
            }
        }
        public IEntitiesViewModel<HospitalDepartment> LookUpHospitalDepartments
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (DoctorViewModel x) => x.LookUpHospitalDepartments,
                    getRepositoryFunc: x => x.HospitalDepartments);
            }
        }
        public IEntitiesViewModel<DoctorGroup> LookUpDoctorGroups
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (DoctorViewModel x) => x.LookUpDoctorGroups,
                    getRepositoryFunc: x => x.DoctorGroups);
            }
        }

        public CollectionViewModelBase<MedicalAppointment, MedicalAppointment, int, IClinicDataModelUnitOfWork> DoctorMedicalAppointmentsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (DoctorViewModel x) => x.DoctorMedicalAppointmentsDetails,
                    getRepositoryFunc: x => x.MedicalAppointments,
                    foreignKeyExpression: x => x.DoctorID,
                    navigationExpression: x => x.Doctor);
            }
        }
    }
}
