﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;

namespace CMS.ViewModels
{
    public partial class ExpenseCollectionViewModel : CollectionViewModel<Expense, int, IClinicDataModelUnitOfWork>, ISupportFiltering<Expense>
    {
        public static ExpenseCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExpenseCollectionViewModel(unitOfWorkFactory));
        }
        protected ExpenseCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Expenses)
        {
        }
        Expression<Func<Expense, bool>> ISupportFiltering<Expense>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<Expense>());
        }
    }
}