﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using System;

namespace CMS.ViewModels
{

    public partial class ExpenseViewModel : SingleObjectViewModel<Expense, int, IClinicDataModelUnitOfWork>
    {

        public static ExpenseViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExpenseViewModel(unitOfWorkFactory));
        }
        protected ExpenseViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Expenses, x => x.ExpenseID)
        {
        }
        protected override Expense CreateEntity()
        {
            Expense Expense = base.CreateEntity();
            Expense.CreatorID = UnitOfWorkSource.CurrentUser.UserID;
            Expense.LastEditorID = UnitOfWorkSource.CurrentUser.UserID;
            Expense.CreationDate = DateTime.Now;
            Expense.LastEditDate = DateTime.Now;
            return Expense;
        }
        public void ShowChanges()
        {
            this.RaisePropertiesChanged();
        }
        public IEntitiesViewModel<StockItem> LookUpStockItems
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (ExpenseViewModel x) => x.LookUpStockItems,
                    getRepositoryFunc: x => x.StockItems);
            }
        }
        public IEntitiesViewModel<Supplier> LookUpSuppliers
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (ExpenseViewModel x) => x.LookUpSuppliers,
                    getRepositoryFunc: x => x.Suppliers);
            }
        }
        public IEntitiesViewModel<User> LookUpUsers
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (ExpenseViewModel x) => x.LookUpUsers,
                    getRepositoryFunc: x => x.Users);
            }
        }
        public CollectionViewModelBase<ExpenseItem, ExpenseItem, int, IClinicDataModelUnitOfWork> ExpenseExpenseItemsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (ExpenseViewModel x) => x.ExpenseExpenseItemsDetails,
                    getRepositoryFunc: x => x.ExpenseItems,
                    foreignKeyExpression: x => x.ExpenseID,
                    navigationExpression: x => x.Expense);
            }
        }
    }
}
