﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.ViewModel;
using CMS.Localization;
using CMS.ClinicDataModelDataModel;
using CMS.Filtering.ViewModels;
using System.Windows.Media;
using DevExpress.Xpf.Core;

namespace CMS.ViewModels
{
    public partial class ClinicDataModelViewModel : DocumentsViewModel<ClinicDataModelModuleDescription, IClinicDataModelUnitOfWork>
    {

        const string TablesGroup = "Tables";
        const string ConfigurationsGroup = "Configurations";
        const string StockGroup = "Stock";
        INavigationService NavigationService { get { return this.GetService<INavigationService>(); } }
        public static ClinicDataModelViewModel Create()
        {
            return ViewModelSource.Create(() => new ClinicDataModelViewModel());
        }
        static ClinicDataModelViewModel()
        {
            MetadataLocator.Default = MetadataLocator.Create().AddMetadata<ClinicDataModelMetadataProvider>();
        }
        protected ClinicDataModelViewModel()
            : base(UnitOfWorkSource.GetUnitOfWorkFactory())
        {
            ConfigurationModules = CreateConfigurationModules();
            StockModules = CreateStockModules();
        }
        public ClinicDataModelModuleDescription[] ConfigurationModules { get; set; }
        public ClinicDataModelModuleDescription[] StockModules { get; set; }

        protected ClinicDataModelModuleDescription[] CreateStockModules()
        {
            return new ClinicDataModelModuleDescription[] {

                new ClinicDataModelModuleDescription("Suppliers","pack://application:,,,/CMS;component/Images/Modules/Supplier/Supplier_32.png", "SupplierCollectionView", StockGroup, null),
                new ClinicDataModelModuleDescription("Stock Items","pack://application:,,,/CMS;component/Images/Modules/StockItem/StockItem_32.png", "StockItemCollectionView", StockGroup, null),
                new ClinicDataModelModuleDescription("Sale Items","pack://application:,,,/CMS;component/Images/Modules/SaleItem/SaleItem_32.png", "SaleItemCollectionView", StockGroup, null),

            };
        }
        protected ClinicDataModelModuleDescription[] CreateConfigurationModules()
        {
            return new ClinicDataModelModuleDescription[] {

                new ClinicDataModelModuleDescription("Appointment Importances","pack://application:,,,/CMS;component/Images/Modules/Importances/Importance_32.png", "AppointmentImportanceCollectionView", ConfigurationsGroup, null),
                new ClinicDataModelModuleDescription("Appointment Locations","pack://application:,,,/CMS;component/Images/Modules/Locations/Location_32.png", "AppointmentLocationCollectionView", ConfigurationsGroup, null),
                new ClinicDataModelModuleDescription("Medication Types","pack://application:,,,/CMS;component/Images/Modules/MedicationTypes/MedicationType_32.png", "MedicationTypeCollectionView", ConfigurationsGroup, null),
                new ClinicDataModelModuleDescription("Payment States","pack://application:,,,/CMS;component/Images/Modules/States/State_32.png", "PaymentStateCollectionView", ConfigurationsGroup, null),
                new ClinicDataModelModuleDescription("Doctor Groups","pack://application:,,,/CMS;component/Images/Modules/Groups/Group_32.png", "DoctorGroupCollectionView", ConfigurationsGroup, null),
                new ClinicDataModelModuleDescription("Hospital Departments","pack://application:,,,/CMS;component/Images/Modules/Departments/Department_32.png", "HospitalDepartmentCollectionView", ConfigurationsGroup, null),
                new ClinicDataModelModuleDescription("Users","pack://application:,,,/CMS;component/Images/Modules/Users/User_32.png", "UserCollectionView", ConfigurationsGroup, null),
                 new ClinicDataModelModuleDescription("Expense Types","pack://application:,,,/CMS;component/Images/Modules/ExpenseTypes/ExpenseType_32.png", "ExpenseTypeCollectionView", ConfigurationsGroup, null)
            };
        }
        protected override ClinicDataModelModuleDescription[] CreateModules()
        {
            return new ClinicDataModelModuleDescription[] {
                new ClinicDataModelModuleDescription(ClinicDataModelResources.ReceptionDesk,"pack://application:,,,/CMS;component/Images/Modules/Reception/Reception_32.png", "ReceptionDeskView", TablesGroup, null),
                new ClinicDataModelModuleDescription(ClinicDataModelResources.PatientPlural,"pack://application:,,,/CMS;component/Images/Modules/Patients/Patient_32.png", "PatientCollectionView", TablesGroup, FiltersSettings.GetPatientsFilterTree(this), GetPeekCollectionViewModelFactory(x => x.Patients)),
                new ClinicDataModelModuleDescription(ClinicDataModelResources.DoctorPlural,"pack://application:,,,/CMS;component/Images/Modules/Doctors/Doctor_32.png", "DoctorCollectionView", TablesGroup, FiltersSettings.GetDoctorsFilterTree(this),GetPeekCollectionViewModelFactory(x => x.Doctors)),
                new ClinicDataModelModuleDescription("Appointments","pack://application:,,,/CMS;component/Images/Modules/Appointments/Appointment_32.png", "MedicalAppointmentCollectionView", TablesGroup, FiltersSettings.GetAppointmentsFilterTree(this), GetPeekCollectionViewModelFactory(x => x.MedicalAppointments)),
                new ClinicDataModelModuleDescription("Invoices","pack://application:,,,/CMS;component/Images/Modules/Invoices/Invoice_32.png", "InvoiceCollectionView", TablesGroup, FiltersSettings.GetInvoicesFilterTree(this), GetPeekCollectionViewModelFactory(x => x.Invoices)),
                new ClinicDataModelModuleDescription("Expenses","pack://application:,,,/CMS;component/Images/Modules/Expenses/Expense_32.png", "ExpenseCollectionView", TablesGroup, FiltersSettings.GetExpensesFilterTree(this), GetPeekCollectionViewModelFactory(x => x.Expenses)),
                new ClinicDataModelModuleDescription("Accounting","pack://application:,,,/CMS;component/Images/Modules/Accounting/Accounting_32.png", "AccountingCollectionView", TablesGroup, null),
               
            };
        }
        protected override void OnActiveModuleChanged(ClinicDataModelModuleDescription oldModule)
        {
            if (ActiveModule != null && NavigationService != null)
            {
                NavigationService.ClearNavigationHistory();
                if (ActiveModule != null && ActiveModule.FilterTreeViewModel != null)
                    ActiveModule.FilterTreeViewModel.SetViewModel(DocumentManagerService.ActiveDocument.Content);
            }
            base.OnActiveModuleChanged(oldModule);
        }
    }

    public partial class ClinicDataModelModuleDescription : ModuleDescription<ClinicDataModelModuleDescription>
    {
        public ClinicDataModelModuleDescription(string title, string uri, string documentType, string group, Func<ClinicDataModelModuleDescription, object> peekCollectionViewModelFactory = null)
            : base(title, documentType, group, peekCollectionViewModelFactory)
        {
            ImageSource = new Uri(string.Format(uri));
        }
        public ClinicDataModelModuleDescription(string title, string uri, string documentType, string group, IFilterTreeViewModel filterTreeViewModel, Func<ClinicDataModelModuleDescription, object> peekCollectionViewModelFactory = null)
          : base(title, documentType, group, peekCollectionViewModelFactory)
        {

            ImageSource = new Uri(string.Format(uri));
            FilterTreeViewModel = filterTreeViewModel;
        }


        public Uri ImageSource { get; private set; }


        public IFilterTreeViewModel FilterTreeViewModel { get; private set; }
    }
}
//public ImageSource ImageSource { get; private set; }
//ImageSource = (ImageSource)new SvgImageSourceExtension() { Uri = new Uri(string.Format(@"pack://application:,,,/CMS;component/Images/Modules/{0}.svg", title)), Size = new System.Windows.Size(24, 24) }
//.ProvideValue(null); 