﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{
    public partial class SupplierViewModel : SingleObjectViewModel<Supplier, int, IClinicDataModelUnitOfWork>
    {

        public static SupplierViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new SupplierViewModel(unitOfWorkFactory));
        }

        protected SupplierViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Suppliers, x => x.SupplierID)
        {
        }
        public IEntitiesViewModel<Invoice> LookUpInvoices
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SupplierViewModel x) => x.LookUpInvoices,
                    getRepositoryFunc: x => x.Invoices);
            }
        }
        public CollectionViewModelBase<Invoice, Invoice, int, IClinicDataModelUnitOfWork> SupplierInvoicesDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (SupplierViewModel x) => x.SupplierInvoicesDetails,
                    getRepositoryFunc: x => x.Invoices,
                    foreignKeyExpression: x => x.SupplierID,
                    navigationExpression: x => x.Supplier);
            }
        }
    }
}
