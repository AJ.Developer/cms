﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class SaleItemViewModel : SingleObjectViewModel<SaleItem, int, IClinicDataModelUnitOfWork>
    {

        public static SaleItemViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new SaleItemViewModel(unitOfWorkFactory));
        }

        protected SaleItemViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.SaleItems, x => x.SaleItemID)
        {
        }
        public IEntitiesViewModel<SaleUnit> LookUpSaleUnits
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SaleItemViewModel x) => x.LookUpSaleUnits,
                    getRepositoryFunc: x => x.SaleUnits);
            }
        }
        public CollectionViewModelBase<SaleUnit, SaleUnit, int, IClinicDataModelUnitOfWork> SaleItemSaleUnitsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (SaleItemViewModel x) => x.SaleItemSaleUnitsDetails,
                    getRepositoryFunc: x => x.SaleUnits,
                    foreignKeyExpression: x => x.SaleItemID,
                    navigationExpression: x => x.SaleItem);
            }
        }
    }
}
