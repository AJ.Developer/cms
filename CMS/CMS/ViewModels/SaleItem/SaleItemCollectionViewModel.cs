﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{
    public partial class SaleItemCollectionViewModel : CollectionViewModel<SaleItem, int, IClinicDataModelUnitOfWork>
    {
        public static SaleItemCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new SaleItemCollectionViewModel(unitOfWorkFactory));
        }
        protected SaleItemCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.SaleItems)
        {
        }

    }
}