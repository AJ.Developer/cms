﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using CMS.Filtering.ViewModels;
using System.Linq.Expressions;

namespace CMS.ViewModels
{
    public partial class UserCollectionViewModel : CollectionViewModel<User, int, IClinicDataModelUnitOfWork>, ISupportFiltering<User>
    {
        public static UserCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new UserCollectionViewModel(unitOfWorkFactory));
        }

        protected UserCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Users)
        {
        }
        Expression<Func<User, bool>> ISupportFiltering<User>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<User>());
        }
    }
}