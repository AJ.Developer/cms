﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using System;
using System.Linq;
using System.Collections.ObjectModel;
using DevExpress.Mvvm.Native;
using System.Collections.Generic;

namespace CMS.ViewModels
{

    public partial class MedicalAppointmentViewModel : SingleObjectViewModel<MedicalAppointment, int, IClinicDataModelUnitOfWork>
    {

        public static MedicalAppointmentViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new MedicalAppointmentViewModel(unitOfWorkFactory));
        }

        protected MedicalAppointmentViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.MedicalAppointments, x => x.MedicalAppointmentID)
        {
            SaleItems = this.UnitOfWork.SaleItems.Where(x => x.IsAvailable).ToObservableCollection();
        }
        public override int PopUpHeight { get; set; } = 800;
        public override int PopUpWidth { get; set; } = 800;
        bool loaded = false;
        public virtual ObservableCollection<SaleItem> SaleItems { get; set; }
        public override void OnLoaded()
        {
            base.OnLoaded();
            loaded = true;
        }
        protected override MedicalAppointment CreateEntity()
        {
            MedicalAppointment MedicalAppointment = base.CreateEntity();
            MedicalAppointment.CreatorID = UnitOfWorkSource.CurrentUser.UserID;
            MedicalAppointment.LastEditorID = UnitOfWorkSource.CurrentUser.UserID;
            MedicalAppointment.CreationDate = DateTime.Now;
            MedicalAppointment.LastEditDate = DateTime.Now;
            MedicalAppointment.StartTime = DateTime.Now;
            MedicalAppointment.EndTime = DateTime.Now.AddMinutes(15);
            return MedicalAppointment;
        }
        public void PatientChanged(DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (loaded)
            {
                if (LookUpPatients.Entities != null && LookUpPatients.Entities.Count > 0 && e.NewValue != null)
                {
                    long? patientID = int.Parse(e.NewValue.ToString());
                    Patient p = LookUpPatients.Entities.Where(x => x.PatientID == patientID).FirstOrDefault();
                    if (p != null)
                        this.Entity.PatientName = p.Name;
                }
                else
                {
                    this.Entity.PatientName = "";
                }
            }
        }
        public void LocationChanged(DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (loaded)
            {
                if (LookUpAppointmentLocations.Entities != null && LookUpAppointmentLocations.Entities.Count > 0 && e.NewValue != null)
                {
                    long? appointmentLocationID = int.Parse(e.NewValue.ToString());
                    AppointmentLocation p = LookUpAppointmentLocations.Entities.Where(x => x.AppointmentLocationID == appointmentLocationID).FirstOrDefault();
                    if (p != null)
                        this.Entity.AppointmentLocationCaption = p.Caption;
                }
                else
                {
                    this.Entity.AppointmentLocationCaption = "";
                }
            }
        }
        public void TypeChanged(DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (loaded)
            {
                if (LookUpMedicationTypes.Entities != null && LookUpMedicationTypes.Entities.Count > 0 && e.NewValue != null)
                {
                    long? medicationTypeID = int.Parse(e.NewValue.ToString());
                    MedicationType p = LookUpMedicationTypes.Entities.Where(x => x.MedicationTypeID == medicationTypeID).FirstOrDefault();
                    if (p != null)
                        this.Entity.MedicationTypeDescription = p.Description;
                }
                else
                {
                    this.Entity.MedicationTypeDescription = "";
                }
            }
        }
        public IEntitiesViewModel<AppointmentLocation> LookUpAppointmentLocations
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (MedicalAppointmentViewModel x) => x.LookUpAppointmentLocations,
                    getRepositoryFunc: x => x.AppointmentLocations);
            }
        }
        public IEntitiesViewModel<Doctor> LookUpDoctors
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (MedicalAppointmentViewModel x) => x.LookUpDoctors,
                    getRepositoryFunc: x => x.Doctors);
            }
        }
        public IEntitiesViewModel<Patient> LookUpPatients
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (MedicalAppointmentViewModel x) => x.LookUpPatients,
                    getRepositoryFunc: x => x.Patients);
            }
        }
        public IEntitiesViewModel<PaymentState> LookUpPaymentStates
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (MedicalAppointmentViewModel x) => x.LookUpPaymentStates,
                    getRepositoryFunc: x => x.PaymentStates);
            }
        }
        public IEntitiesViewModel<AppointmentImportance> LookUpAppointmentImportances
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (MedicalAppointmentViewModel x) => x.LookUpAppointmentImportances,
                    getRepositoryFunc: x => x.AppointmentImportances);
            }
        }
        public IEntitiesViewModel<MedicationType> LookUpMedicationTypes
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (MedicalAppointmentViewModel x) => x.LookUpMedicationTypes,
                    getRepositoryFunc: x => x.MedicationTypes);
            }
        }
        public IEntitiesViewModel<SaleItem> LookUpSaleItems
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (MedicalAppointmentViewModel x) => x.LookUpSaleItems,
                    getRepositoryFunc: x => x.SaleItems);
            }
        }
        public virtual SaleItem SelectedSaleItem { get; set; }
        public virtual int Quantity { get; set; }
        public void AddToTreatments()
        {
            if (this.Entity.MedicalAppointmentID == 0)
                Save();
            if (this.Entity.Treatments == null)
                this.Entity.Treatments = new ObservableCollection<Treatment>();
            if (this.Entity != null)
            {
                Treatment Treatment = this.Entity.Treatments.Where(x => x.SaleItemID == SelectedSaleItem.SaleItemID).FirstOrDefault();
                if (Treatment == null)
                {
                    Treatment = new Treatment { Quantity = Quantity, Price = SelectedSaleItem.Price, SaleItemID = SelectedSaleItem.SaleItemID };
                    Treatment.SaleItem = UnitOfWork.SaleItems.Where(x => x.SaleItemID == SelectedSaleItem.SaleItemID).FirstOrDefault();
                    Treatment.MyQuantity = Quantity;
                    this.Entity.Treatments.Add(Treatment);
                }
                else
                {
                    Treatment.Quantity += Quantity;
                    Treatment.MyQuantity = Treatment.Quantity;
                }
                List<SaleUnit> saleUnits = Treatment.SaleItem.SaleUnits.ToList();
                foreach (SaleUnit su in saleUnits)
                {
                    if (su.StockItem.IsConsumable)
                        UnitOfWork.StockItems.Where(x => x.StockItemID == su.StockItem.StockItemID).FirstOrDefault().Quantity -= (su.Quantity * Quantity);
                }
                base.Save();
            }

        }
        public void DeleteTreatment(Treatment Treatment)
        {
            List<SaleUnit> saleUnits = Treatment.SaleItem.SaleUnits.ToList();
            foreach (SaleUnit su in saleUnits)
            {
                if (su.StockItem.IsConsumable)
                    UnitOfWork.StockItems.Where(x => x.StockItemID == su.StockItem.StockItemID).FirstOrDefault().Quantity += (su.Quantity * Treatment.Quantity);
            }
            this.Entity.Treatments.Remove(Treatment);
            UnitOfWork.Treatments.Remove(Treatment);
            base.Save();
        }
        public void UpdateItemQuantity(Treatment Treatment)
        {

            int Qdiff = Treatment.MyQuantity - Treatment.Quantity;

            List<SaleUnit> saleUnits = Treatment.SaleItem.SaleUnits.ToList();
            foreach (SaleUnit su in saleUnits)
            {
                if (su.StockItem.IsConsumable)
                    UnitOfWork.StockItems.Where(x => x.StockItemID == su.StockItem.StockItemID).FirstOrDefault().Quantity += (Qdiff * su.Quantity);

            }
            Treatment.MyQuantity = Treatment.Quantity;

            base.Save();
        }
        public bool CanAddToTreatments()
        {
            return SelectedSaleItem != null && Quantity > 0;
        }
    }
}
