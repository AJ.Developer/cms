﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;
using DevExpress.Mvvm;
using System.Collections.Generic;
using DevExpress.Data.Filtering;

namespace CMS.ViewModels
{
    public partial class MedicalAppointmentCollectionViewModel : CollectionViewModel<MedicalAppointment, int, IClinicDataModelUnitOfWork>, ISupportFiltering<MedicalAppointment>, IFilterTreeViewModelContainer<MedicalAppointment, int>
    {
        public static MedicalAppointmentCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new MedicalAppointmentCollectionViewModel(unitOfWorkFactory));
        }
        protected MedicalAppointmentCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.MedicalAppointments)
        {
        }
       
        Expression<Func<MedicalAppointment, bool>> ISupportFiltering<MedicalAppointment>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<MedicalAppointment>());
        }
        public FilterTreeViewModel<MedicalAppointment, int> FilterTreeViewModel { get; set; }
        public override void OnLoaded()
        {
            base.OnLoaded();
            if (FilterTreeViewModel == null) return;
            DeleteDoctorFilters();
            CreateDoctorFilters();
        }
        void CreateDoctorFilters()
        {
            UnitOfWorkFactory.CreateUnitOfWork().MedicalAppointments
                .Where(x => x.DoctorID != null && x.Doctor != null && !string.IsNullOrEmpty(x.Doctor.Name))
                .Select(x => new FilterInfo() { FilterCriteria = "[DoctorID]" + "=" + x.Doctor.DoctorID, Name = "With Dr." + x.Doctor.Name })
                .Distinct().ToList()
                .ForEach(x => AddDoctorFilter(x));
        }
        void DeleteDoctorFilters()
        {
            FilterTreeViewModel.CustomFilters.Where(x => IsDoctorFilter(x))
            .ToList()
            .ForEach(x => FilterTreeViewModel.DeleteCustomFilter(x));
        }
        void AddDoctorFilter(FilterInfo filterInfo)
        {
            FilterTreeViewModel.AddCustomFilter(filterInfo.Name, DevExpress.Data.Filtering.CriteriaOperator.Parse(filterInfo.FilterCriteria));
        }
        bool IsDoctorFilter(FilterItem filterItem)
        {
            return filterItem.Name.StartsWith("With");
            //return filterItem.FilterCriteria.LegacyToString().Contains("[DoctorID]");
        }
    }
}