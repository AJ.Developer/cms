﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Xpf.Scheduling;

namespace CMS.ViewModels
{

    public partial class AppointmentImportanceViewModel : SingleObjectViewModel<AppointmentImportance, int, IClinicDataModelUnitOfWork>
    {

        public static AppointmentImportanceViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AppointmentImportanceViewModel(unitOfWorkFactory));
        }

        protected AppointmentImportanceViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AppointmentImportances, x => x.AppointmentImportanceID)
        {
        }

        public List<string> StatusBrushs { get; set; } = new List<string> { "Transparent", "Red", "Olive", "Green", "Orange" };

        public IEntitiesViewModel<MedicalAppointment> LookUpMedicalAppointments
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (AppointmentImportanceViewModel x) => x.LookUpMedicalAppointments,
                    getRepositoryFunc: x => x.MedicalAppointments);
            }
        }
        public CollectionViewModelBase<MedicalAppointment, MedicalAppointment, int, IClinicDataModelUnitOfWork> AppointmentImportanceMedicalAppointmentsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (AppointmentImportanceViewModel x) => x.AppointmentImportanceMedicalAppointmentsDetails,
                    getRepositoryFunc: x => x.MedicalAppointments,
                    foreignKeyExpression: x => x.AppointmentImportanceID,
                    navigationExpression: x => x.AppointmentImportance);
            }
        }
    }
}
