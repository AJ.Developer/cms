﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using CMS.Filtering.ViewModels;
using System.Linq.Expressions;
using DevExpress.Mvvm;

namespace CMS.ViewModels
{
    public partial class AppointmentImportanceCollectionViewModel : CollectionViewModel<AppointmentImportance, int, IClinicDataModelUnitOfWork>, ISupportFiltering<AppointmentImportance>
    {
        public static AppointmentImportanceCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AppointmentImportanceCollectionViewModel(unitOfWorkFactory));
        }
        protected AppointmentImportanceCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AppointmentImportances)
        {
        }
        Expression<Func<AppointmentImportance, bool>> ISupportFiltering<AppointmentImportance>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<AppointmentImportance>());
        }
    }
}