﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;

namespace CMS.ViewModels
{
    public partial class AppointmentsStatisticsCollectionViewModel : CollectionViewModel<MedicalAppointment, int, IClinicDataModelUnitOfWork>, ISupportFiltering<MedicalAppointment>
    {
        public static AppointmentsStatisticsCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AppointmentsStatisticsCollectionViewModel(unitOfWorkFactory));
        }
        protected AppointmentsStatisticsCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.MedicalAppointments)
        {
        }
        Expression<Func<MedicalAppointment, bool>> ISupportFiltering<MedicalAppointment>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<Expense>());
        }
    }
}