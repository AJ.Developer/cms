﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;

namespace CMS.ViewModels
{
    public partial class InvoicesStatisticsCollectionViewModel : CollectionViewModel<Invoice, int, IClinicDataModelUnitOfWork>, ISupportFiltering<Invoice>
    {
        public static InvoicesStatisticsCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new InvoicesStatisticsCollectionViewModel(unitOfWorkFactory));
        }
        protected InvoicesStatisticsCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Invoices)
        {
        }
        Expression<Func<Invoice, bool>> ISupportFiltering<Invoice>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<Expense>());
        }
    }
}