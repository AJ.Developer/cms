﻿using CMS.ClinicDataModelDataModel;
using CMS.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.ViewModels
{
    public class InvoicesDashboard
    {
        public class DataItem
        {
            decimal invoice;
            DateTime curtDate;
            string type;
            string creator;

            public string Creator
            {
                get { return creator; }
                set { creator = value; }
            }
            public string Type
            {
                get { return type; }
                set { type = value; }
            }
            public DateTime CurrentDate
            {
                get { return curtDate; }
                set { curtDate = value; }
            }

            public decimal Invoices
            {
                get { return invoice; }
                set { invoice = value; }
            }

        }

        public class DataKey
        {
            readonly string creator;
            readonly string type;
            readonly DateTime dt;

            public DataKey(string creator, string type, DateTime dt)
            {
                this.creator = creator;
                this.type = type;
                this.dt = dt;
            }
            public override bool Equals(object obj)
            {
                DataKey key = (DataKey)obj;
                return key.creator == creator && key.type == type && key.dt == dt;
            }
            public override int GetHashCode()
            {
                return creator.GetHashCode() ^ type.GetHashCode() ^ dt.GetHashCode();
            }
        }

        readonly Dictionary<DataKey, DataItem> dat = new Dictionary<DataKey, DataItem>();
        readonly DateTime startDate;
        readonly DateTime endDate;

        public IEnumerable<DataItem> Data { get { return dat.Values; } }

        public InvoicesDashboard()
        {
            endDate = DateTime.Today; startDate = new DateTime(endDate.Year - 2, 1, 1);
            //startDate = endDate.AddDays(-7);
        }
        public void Generate()
        {
            IClinicDataModelUnitOfWork dBModelUnitOfWork = UnitOfWorkSource.GetUnitOfWorkFactory().CreateUnitOfWork();
            if (dBModelUnitOfWork != null && dBModelUnitOfWork.Invoices != null)
            {
                List<Invoice> Invoices = dBModelUnitOfWork.Invoices.Where(x => x.InvoiceStatus == InvoiceStatus.Done).ToList();
                foreach (Invoice Invoice in Invoices)
                {
                    foreach (InvoiceItem InvoiceItem in Invoice.InvoiceItems)
                    {
                        DataKey datKey = new DataKey(Invoice.I_Creator.Username, InvoiceItem.StockItem.Name, Invoice.CreationDate.Value);
                        DataItem datItem = null;
                        if (!dat.TryGetValue(datKey, out datItem))
                        {
                            datItem = new DataItem
                            {
                                CurrentDate = Invoice.CreationDate.Value,
                                Type = InvoiceItem.StockItem.Name,
                                Creator = Invoice.I_Creator.Username,
                            };
                            dat.Add(datKey, datItem);
                        }

                        datItem.Invoices += InvoiceItem.TotalPrice;
                    }
                }
            }
        }
    }
}
