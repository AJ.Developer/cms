﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using CMS.Filtering.ViewModels;
using System.Linq.Expressions;
using DevExpress.Mvvm;

namespace CMS.ViewModels
{
    public partial class AccountingCollectionViewModel : CollectionViewModel<AppointmentImportance, int, IClinicDataModelUnitOfWork>
    {
        public static AccountingCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AccountingCollectionViewModel(unitOfWorkFactory));
        }
        protected AccountingCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AppointmentImportances)
        {
        }

    }
}