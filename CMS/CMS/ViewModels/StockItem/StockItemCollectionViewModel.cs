﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;

namespace CMS.ViewModels
{
    public partial class StockItemCollectionViewModel : CollectionViewModel<StockItem, int, IClinicDataModelUnitOfWork>
    {
        public static StockItemCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new StockItemCollectionViewModel(unitOfWorkFactory));
        }
        protected StockItemCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.StockItems)
        {
        }
       
    }
}