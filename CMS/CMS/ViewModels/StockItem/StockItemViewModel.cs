﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class StockItemViewModel : SingleObjectViewModel<StockItem, int, IClinicDataModelUnitOfWork>
    {

        public static StockItemViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new StockItemViewModel(unitOfWorkFactory));
        }

        protected StockItemViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.StockItems, x => x.StockItemID)
        {
        }
        protected override StockItem CreateEntity()
        {
            StockItem StockItem = base.CreateEntity();
            StockItem.IsConsumable = true;
            return StockItem;
        }
    }
}
