﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;

namespace CMS.ViewModels
{
    public partial class HospitalDepartmentCollectionViewModel : CollectionViewModel<HospitalDepartment, int, IClinicDataModelUnitOfWork>, ISupportFiltering<HospitalDepartment>
    {
        public static HospitalDepartmentCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new HospitalDepartmentCollectionViewModel(unitOfWorkFactory));
        }
        protected HospitalDepartmentCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.HospitalDepartments)
        {
        }
        Expression<Func<HospitalDepartment, bool>> ISupportFiltering<HospitalDepartment>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<HospitalDepartment>());
        }
    }
}