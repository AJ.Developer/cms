﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class HospitalDepartmentViewModel : SingleObjectViewModel<HospitalDepartment, int, IClinicDataModelUnitOfWork>
    {

        public static HospitalDepartmentViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new HospitalDepartmentViewModel(unitOfWorkFactory));
        }

        protected HospitalDepartmentViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.HospitalDepartments, x => x.HospitalDepartmentID)
        {
        }

        public IEntitiesViewModel<Doctor> LookUpDoctors
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (HospitalDepartmentViewModel x) => x.LookUpDoctors,
                    getRepositoryFunc: x => x.Doctors);
            }
        }

        public CollectionViewModelBase<Doctor, Doctor, int, IClinicDataModelUnitOfWork> HospitalDepartmentDoctorsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (HospitalDepartmentViewModel x) => x.HospitalDepartmentDoctorsDetails,
                    getRepositoryFunc: x => x.Doctors,
                    foreignKeyExpression: x => x.HospitalDepartmentID,
                    navigationExpression: x => x.HospitalDepartment);
            }
        }
    }
}
