﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class AppointmentLocationViewModel : SingleObjectViewModel<AppointmentLocation, int, IClinicDataModelUnitOfWork>
    {

        public static AppointmentLocationViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AppointmentLocationViewModel(unitOfWorkFactory));
        }

        protected AppointmentLocationViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AppointmentLocations, x => x.AppointmentLocationID)
        {
        }

        public IEntitiesViewModel<MedicalAppointment> LookUpMedicalAppointments
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (AppointmentLocationViewModel x) => x.LookUpMedicalAppointments,
                    getRepositoryFunc: x => x.MedicalAppointments);
            }
        }
        public CollectionViewModelBase<MedicalAppointment, MedicalAppointment, int, IClinicDataModelUnitOfWork> AppointmentLocationMedicalAppointmentsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (AppointmentLocationViewModel x) => x.AppointmentLocationMedicalAppointmentsDetails,
                    getRepositoryFunc: x => x.MedicalAppointments,
                    foreignKeyExpression: x => x.AppointmentLocationID,
                    navigationExpression: x => x.AppointmentLocation);
            }
        }
    }
}
