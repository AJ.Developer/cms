﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;
using DevExpress.Mvvm;

namespace CMS.ViewModels {


    public partial class AppointmentLocationCollectionViewModel : CollectionViewModel<AppointmentLocation, int, IClinicDataModelUnitOfWork>, ISupportFiltering<AppointmentLocation>
    {
        public static AppointmentLocationCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new AppointmentLocationCollectionViewModel(unitOfWorkFactory));
        }

        protected AppointmentLocationCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AppointmentLocations) {
        }
        Expression<Func<AppointmentLocation, bool>> ISupportFiltering<AppointmentLocation>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<AppointmentLocation>());
        }
    }
}