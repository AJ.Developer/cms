﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using CMS.Filtering.ViewModels;
using System.Linq.Expressions;
using DevExpress.Mvvm;

namespace CMS.ViewModels
{
    public partial class ExpenseTypeCollectionViewModel : CollectionViewModel<ExpenseType, int, IClinicDataModelUnitOfWork>, ISupportFiltering<ExpenseType>
    {

        public static ExpenseTypeCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExpenseTypeCollectionViewModel(unitOfWorkFactory));
        }

        protected ExpenseTypeCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.ExpenseTypes)
        {
        }
     
    }
}