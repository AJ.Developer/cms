﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{
    public partial class ExpenseTypeViewModel : SingleObjectViewModel<ExpenseType, int, IClinicDataModelUnitOfWork>
    {
        public static ExpenseTypeViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExpenseTypeViewModel(unitOfWorkFactory));
        }

        protected ExpenseTypeViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.ExpenseTypes, x => x.ExpenseTypeID)
        {
        }
      
    }
}
