﻿using System;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using System.Collections.ObjectModel;
using System.Windows.Input;
using DevExpress.Mvvm;
using DevExpress.Mvvm.Utils;
using System.Linq.Expressions;
using CMS.Filtering.ViewModels;
using DevExpress.Xpf.Scheduling;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Mvvm.Native;

namespace CMS.ViewModels
{
    public class ReceptionDeskViewModel : CollectionViewModel<MedicalAppointment, int, IClinicDataModelUnitOfWork>, ISupportFiltering<MedicalAppointment>
    {
        public static ReceptionDeskViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ReceptionDeskViewModel(unitOfWorkFactory));
        }
        protected ReceptionDeskViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.MedicalAppointments)
        {
            clinicDataModel = new ClinicDataModel();
            clinicDataModel.MedicalAppointments.Load();
            clinicDataModel.Doctors.Load();
            clinicDataModel.Patients.Load();
            clinicDataModel.AppointmentLocations.Load();
            clinicDataModel.PaymentStates.Load();
            clinicDataModel.AppointmentImportances.Load();
            clinicDataModel.HospitalDepartments.Load();
            clinicDataModel.StockItems.Load();
            clinicDataModel.Treatments.Load();
        }
        ClinicDataModel clinicDataModel;

        public void SaveAll()
        {
            clinicDataModel.SaveChanges();
        }

        public void RemoveTreatments(AppointmentRemovingEventArgs e)
        {
            clinicDataModel.Treatments.Load();
            clinicDataModel.StockItems.Load();
            foreach (AppointmentItem app in e.Appointments)
            {
                ObservableCollection<Treatment> appointmentTreatments;
                appointmentTreatments = Treatments.Where(x => x.MedicalAppointmentID == (int)app.Id)?.ToObservableCollection();
                if (appointmentTreatments != null && appointmentTreatments.Count > 0)
                {
                    foreach (Treatment treatment in appointmentTreatments)
                    {
                        List<SaleUnit> saleUnits = treatment.SaleItem.SaleUnits.ToList();
                        foreach (SaleUnit su in saleUnits)
                        {
                            if (su.StockItem.IsConsumable)
                            {
                                StockItem stockItem = StockItems.Where(x => x.StockItemID == su.StockItem.StockItemID).FirstOrDefault();
                                clinicDataModel.Entry(stockItem).Reload();

                                stockItem.Quantity += (su.Quantity * treatment.Quantity);
                            }
                        }

                        clinicDataModel.Treatments.Remove(treatment);
                        clinicDataModel.SaveChanges();

                    }
                }
            }
        }
        public DateTime StartDate { get { return DateTime.Now; } }
        public virtual ObservableCollection<Doctor> Doctors { get { return clinicDataModel.Doctors.Local; } }
        public virtual ObservableCollection<MedicalAppointment> MedicalAppointments { get { return clinicDataModel.MedicalAppointments.Local; } }
        public virtual ObservableCollection<HospitalDepartment> HospitalDepartments { get { return clinicDataModel.HospitalDepartments.Local; } }
        public virtual ObservableCollection<Patient> Patients { get { return clinicDataModel.Patients.Local; } }
        public virtual ObservableCollection<AppointmentLocation> Locations { get { return clinicDataModel.AppointmentLocations.Local; } }
        public virtual ObservableCollection<PaymentState> PaymentStates { get { return clinicDataModel.PaymentStates.Local; } }
        public virtual ObservableCollection<AppointmentImportance> AppointmentImportances { get { return clinicDataModel.AppointmentImportances.Local; } }
        public virtual ObservableCollection<Treatment> Treatments { get { return clinicDataModel.Treatments.Local; } }
        public virtual ObservableCollection<StockItem> StockItems { get { return clinicDataModel.StockItems.Local; } }
        Expression<Func<MedicalAppointment, bool>> ISupportFiltering<MedicalAppointment>.FilterExpression
        {
            get { return FilterExpression; }
            set { FilterExpression = value; }
        }
        public void CreateCustomFilter()
        {
            Messenger.Default.Send(new CreateCustomFilterMessage<MedicalAppointment>());
        }
    }
}
