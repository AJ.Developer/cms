﻿using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.Native;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.Utils;
using DevExpress.Mvvm.ViewModel;
using DevExpress.Xpf.Scheduling;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Windows;

namespace CMS.ViewModels
{
    public class MedicalAppointmentWindowViewModel : AppointmentWindowViewModel
    {
        public static MedicalAppointmentWindowViewModel Create(AppointmentItem appointmentItem, SchedulerControl scheduler)
        {
            return ViewModelSource.Create(() => new MedicalAppointmentWindowViewModel(appointmentItem, scheduler));
        }
        protected MedicalAppointmentWindowViewModel(AppointmentItem appointmentItem, SchedulerControl scheduler) : base(appointmentItem, scheduler)
        {
            Quantity = 1;
            clinicDataModel = new ClinicDataModel();
            clinicDataModel.Patients.Load();
            clinicDataModel.Treatments.Load();
            clinicDataModel.Users.Load();
            clinicDataModel.SaleItems.Load();
            clinicDataModel.AppointmentLocations.Load();
            clinicDataModel.PaymentStates.Load();
            clinicDataModel.MedicationTypes.Load();
            clinicDataModel.StockItems.Load();

            User creator = Users.FirstOrDefault(x => x.UserID.Equals(CustomFields["CreatorID"]));
            if (creator == null)
            {
                CustomFields["CreatorID"] = UnitOfWorkSource.CurrentUser.UserID;
                CustomFields["CreationDate"] = DateTime.Now;
            }

            patient = Patients.FirstOrDefault(x => x.PatientID.Equals(CustomFields["PatientID"]));
            MedicationType = MedicationTypes.FirstOrDefault(x => x.MedicationTypeID.Equals(CustomFields["MedicationTypeID"]));
            appointmentLocation = Locations.FirstOrDefault(x => x.AppointmentLocationID.Equals(CustomFields["AppointmentLocationID"]));

            appointmentTreatments = AllTreatments.Where(x => x.MedicalAppointmentID == (int)appointmentItem.Id)?.ToObservableCollection();
            if (appointmentTreatments == null)
                appointmentTreatments = new ObservableCollection<Treatment>();
        }

        ClinicDataModel clinicDataModel;

        public ObservableCollection<SaleItem> SaleItems { get { return clinicDataModel.SaleItems.Local; } }
        public ObservableCollection<User> Users { get { return clinicDataModel.Users.Local; } }
        public ObservableCollection<Patient> Patients { get { return clinicDataModel.Patients.Local; } }
        public ObservableCollection<MedicationType> MedicationTypes { get { return clinicDataModel.MedicationTypes.Local; } }
        public ObservableCollection<AppointmentLocation> Locations { get { return clinicDataModel.AppointmentLocations.Local; } }
        public ObservableCollection<Treatment> AllTreatments { get { return clinicDataModel.Treatments.Local; } }
        public ObservableCollection<PaymentState> PaymentStates { get { return clinicDataModel.PaymentStates.Local; } }
        public ObservableCollection<StockItem> StockItems { get { return clinicDataModel.StockItems.Local; } }

        public virtual SaleItem SelectedSaleItem { get; set; }
        public virtual int Quantity { get; set; }
        public virtual decimal TotalAmount
        {
            get
            {
                if (AppointmentTreatments != null)
                {
                    decimal val = 0;
                    foreach (Treatment t in appointmentTreatments)
                        val += (t.Quantity * t.Price);
                    return val;
                }
                return 0;
            }
        }
        public void AddToTreatments()
        {

            if ((int)Appointment.Id == 0)
                SaveAppointment();
            if (appointmentTreatments == null)
                appointmentTreatments = new ObservableCollection<Treatment>();
            Treatment Treatment = appointmentTreatments.Where(x => x.SaleItemID == SelectedSaleItem.SaleItemID).FirstOrDefault();
            if (Treatment == null)
            {
                Treatment = new Treatment { Quantity = Quantity, MyQuantity = Quantity, Price = SelectedSaleItem.Price, MedicalAppointmentID = (int)Appointment.Id, SaleItem = SelectedSaleItem };
                appointmentTreatments.Add(Treatment);
                clinicDataModel.Treatments.Add(Treatment);
                clinicDataModel.SaveChanges();
            }
            else
            {
                Treatment.Quantity += Quantity;
                Treatment.MyQuantity = Treatment.Quantity;
            }
            List<SaleUnit> saleUnits = Treatment.SaleItem.SaleUnits.ToList();
            foreach (SaleUnit su in saleUnits)
            {
                if (su.StockItem.IsConsumable)
                    StockItems.Where(x => x.StockItemID == su.StockItem.StockItemID).FirstOrDefault().Quantity -= (su.Quantity * Quantity);

            }
            clinicDataModel.SaveChanges();
            this.RaisePropertiesChanged();

        }
        public void DeleteTreatment(Treatment Treatment)
        {
            List<SaleUnit> saleUnits = Treatment.SaleItem.SaleUnits.ToList();
            foreach (SaleUnit su in saleUnits)
            {
                if (su.StockItem.IsConsumable)
                    StockItems.Where(x => x.StockItemID == su.StockItem.StockItemID).FirstOrDefault().Quantity += (su.Quantity * Treatment.Quantity);
            }
            appointmentTreatments.Remove(Treatment);
            clinicDataModel.Treatments.Remove(Treatment);
            clinicDataModel.SaveChanges();
            this.RaisePropertiesChanged();
        }
        public void UpdateItemQuantity(Treatment Treatment)
        {
            int Qdiff = Treatment.MyQuantity - Treatment.Quantity;
            List<SaleUnit> saleUnits = Treatment.SaleItem.SaleUnits.ToList();
            foreach (SaleUnit su in saleUnits)
            {
                if (su.StockItem.IsConsumable)
                    StockItems.Where(x => x.StockItemID == su.StockItem.StockItemID).FirstOrDefault().Quantity += (Qdiff * su.Quantity);
            }
            Treatment.MyQuantity = Treatment.Quantity;
            clinicDataModel.SaveChanges();
            this.RaisePropertiesChanged();
        }
        public bool CanAddToTreatments()
        {
            return SelectedSaleItem != null && Quantity > 0;
        }

        protected override void ApplyChanges(AppointmentItem appointment)
        {
            base.ApplyChanges(appointment);
        }
        protected override void SaveAppointment()
        {
            CustomFields["LastEditorID"] = UnitOfWorkSource.CurrentUser.UserID;
            CustomFields["LastEditDate"] = DateTime.Now;
            base.SaveAppointment();
        }

        ObservableCollection<Treatment> appointmentTreatments;
        [BindableProperty]
        public virtual ObservableCollection<Treatment> AppointmentTreatments
        {
            get { return appointmentTreatments; }
            set
            {
                ObservableCollection<Treatment> newAppointmentTreatments = value;
                if (appointmentTreatments == newAppointmentTreatments)
                    return;
                appointmentTreatments = newAppointmentTreatments;

                CustomFields["Treatments"] = newAppointmentTreatments;
            }
        }

        Patient patient;
        [BindableProperty]
        public virtual Patient Patient
        {
            get { return patient; }
            set
            {
                Patient newPatient = value;
                if (patient == newPatient)
                    return;
                patient = newPatient;

                CustomFields["PatientID"] = newPatient?.PatientID;
                CustomFields["PatientName"] = newPatient?.Name;


            }
        }

        AppointmentLocation appointmentLocation;
        [BindableProperty]
        public virtual AppointmentLocation AppointmentLocation
        {
            get { return appointmentLocation; }
            set
            {
                AppointmentLocation newAppointmentLocation = value;
                if (appointmentLocation == newAppointmentLocation)
                    return;
                appointmentLocation = newAppointmentLocation;

                CustomFields["AppointmentLocationID"] = newAppointmentLocation?.AppointmentLocationID;
                CustomFields["AppointmentLocationCaption"] = newAppointmentLocation?.Caption;

            }
        }

        MedicationType medicationType;
        [BindableProperty]
        public virtual MedicationType MedicationType
        {
            get { return medicationType; }
            set
            {
                MedicationType newMedicationType = value;
                if (medicationType == newMedicationType)
                    return;
                medicationType = newMedicationType;

                CustomFields["MedicationTypeID"] = newMedicationType?.MedicationTypeID;
                CustomFields["MedicationTypeDescription"] = newMedicationType?.Description;

            }
        }

        [ServiceProperty(Key = "AddMedicationTypeService")]
        protected virtual IDialogService AddMedicationTypeService { get { return null; } }
        DialogMedicationTypeViewModel DialogMedicationTypeViewModel = DialogMedicationTypeViewModel.Create();
        public void AddMedicationType()
        {

            DialogMedicationTypeViewModel.Description = "";
            var okCommand = new UICommand
            {
                Caption = "Ok",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(x => { AddMedicationTypeAndRefresh(); }, y => { return !IDataErrorInfoHelper.HasErrors(DialogMedicationTypeViewModel); })
            };
            var cancelCommand = new UICommand()
            {
                Id = MessageBoxResult.Cancel,
                Caption = "Cancel",
                IsCancel = true,
                IsDefault = false
            };

            var result = AddMedicationTypeService.ShowDialog(new List<UICommand> { okCommand, cancelCommand }, "Add New Medication Type", DialogMedicationTypeViewModel);
        }
        private void AddMedicationTypeAndRefresh()
        {
            MedicationType p = new MedicationType { Description = DialogMedicationTypeViewModel.Description };
            clinicDataModel.MedicationTypes.Add(p);
            clinicDataModel.SaveChanges();
            MedicationType = p;

        }

        [ServiceProperty(Key = "AddPatientService")]
        protected virtual IDialogService AddPatientService { get { return null; } }
        DialogPatientViewModel DialogPatientViewModel = DialogPatientViewModel.Create();
        public void AddPatient()
        {

            DialogPatientViewModel.Name = "";
            DialogPatientViewModel.BirthDate = DateTime.Now;
            DialogPatientViewModel.Phone = "";
            var okCommand = new UICommand
            {
                Caption = "Ok",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(x => { AddPatientAndRefresh(); }, y => { return !IDataErrorInfoHelper.HasErrors(DialogPatientViewModel); })
            };
            var cancelCommand = new UICommand()
            {
                Id = MessageBoxResult.Cancel,
                Caption = "Cancel",
                IsCancel = true,
                IsDefault = false
            };

            var result = AddPatientService.ShowDialog(new List<UICommand> { okCommand, cancelCommand }, "Add New Patient", DialogPatientViewModel);
        }
        private void AddPatientAndRefresh()
        {
            Patient p = new Patient { Name = DialogPatientViewModel.Name, BirthDate = DialogPatientViewModel.BirthDate, Phone = DialogPatientViewModel.Phone };
            clinicDataModel.Patients.Add(p);
            clinicDataModel.SaveChanges();
            Patient = p;

        }
    }
    public partial class DialogMedicationTypeViewModel : SingleObjectViewModel<MedicationType, int, IClinicDataModelUnitOfWork>, IDataErrorInfo
    {
        public static DialogMedicationTypeViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new DialogMedicationTypeViewModel(unitOfWorkFactory));
        }

        protected DialogMedicationTypeViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.MedicationTypes, x => x.MedicationTypeID)
        {

        }
        public virtual string Description { get; set; }
        [NotMapped]
        public string Error { get; set; } = "";
        public string this[string columnName]
        {
            get
            {

                switch (columnName)
                {
                    case "Description":
                        if (string.IsNullOrEmpty(this.Description))
                            return "Desciption Is Required";
                        break;

                    default: return string.Empty;
                }
                return string.Empty;
            }
        }
    }
    public partial class DialogPatientViewModel : SingleObjectViewModel<Patient, int, IClinicDataModelUnitOfWork>, IDataErrorInfo
    {
        public static DialogPatientViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new DialogPatientViewModel(unitOfWorkFactory));
        }

        protected DialogPatientViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Patients, x => x.PatientID)
        {

        }
        public string MyTitle { get; set; }
        protected override string GetTitle()
        {
            return MyTitle;
        }

        public virtual string Name { get; set; }
        public virtual DateTime BirthDate { get; set; }
        public virtual string Phone { get; set; }
        [NotMapped]
        public string Error { get; set; } = "";
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Name":
                        if (string.IsNullOrEmpty(this.Name))
                            return "Name Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }
    }
}
