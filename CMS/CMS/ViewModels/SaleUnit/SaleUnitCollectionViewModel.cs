﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{
    public partial class SaleUnitCollectionViewModel : CollectionViewModel<SaleUnit, int, IClinicDataModelUnitOfWork>
    {
        public static SaleUnitCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new SaleUnitCollectionViewModel(unitOfWorkFactory));
        }
        protected SaleUnitCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.SaleUnits)
        {
        }
    }
}