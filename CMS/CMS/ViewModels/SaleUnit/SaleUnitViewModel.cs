﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class SaleUnitViewModel : SingleObjectViewModel<SaleUnit, int, IClinicDataModelUnitOfWork>
    {

        public static SaleUnitViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new SaleUnitViewModel(unitOfWorkFactory));
        }

        protected SaleUnitViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.SaleUnits, x => x.SaleUnitID)
        {
        }
        public IEntitiesViewModel<StockItem> LookUpStockItems
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SaleUnitViewModel x) => x.LookUpStockItems,
                    getRepositoryFunc: x => x.StockItems);
            }
        }
    }
}
