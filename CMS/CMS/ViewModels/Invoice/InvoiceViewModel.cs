﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;
using System;

namespace CMS.ViewModels
{

    public partial class InvoiceViewModel : SingleObjectViewModel<Invoice, int, IClinicDataModelUnitOfWork>
    {

        public static InvoiceViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new InvoiceViewModel(unitOfWorkFactory));
        }
        protected InvoiceViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Invoices, x => x.InvoiceID)
        {
        }
        protected override Invoice CreateEntity()
        {
            Invoice invoice = base.CreateEntity();
            invoice.CreatorID = UnitOfWorkSource.CurrentUser.UserID;
            invoice.LastEditorID = UnitOfWorkSource.CurrentUser.UserID;
            invoice.CreationDate = DateTime.Now;
            invoice.LastEditDate = DateTime.Now;
            return invoice;
        }
        public void ShowChanges()
        {
            this.RaisePropertiesChanged();
        }
        public IEntitiesViewModel<StockItem> LookUpStockItems
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.LookUpStockItems,
                    getRepositoryFunc: x => x.StockItems);
            }
        }
        public IEntitiesViewModel<Supplier> LookUpSuppliers
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.LookUpSuppliers,
                    getRepositoryFunc: x => x.Suppliers);
            }
        }
        public IEntitiesViewModel<User> LookUpUsers
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.LookUpUsers,
                    getRepositoryFunc: x => x.Users);
            }
        }
        public CollectionViewModelBase<InvoiceItem, InvoiceItem, int, IClinicDataModelUnitOfWork> InvoiceInvoiceItemsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.InvoiceInvoiceItemsDetails,
                    getRepositoryFunc: x => x.InvoiceItems,
                    foreignKeyExpression: x => x.InvoiceID,
                    navigationExpression: x => x.Invoice);
            }
        }
    }
}
