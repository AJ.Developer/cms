﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class ExpenseItemViewModel : SingleObjectViewModel<ExpenseItem, int, IClinicDataModelUnitOfWork>
    {

        public static ExpenseItemViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExpenseItemViewModel(unitOfWorkFactory));
        }

        protected ExpenseItemViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.ExpenseItems, x => x.ExpenseItemID)
        {
        }
        public IEntitiesViewModel<ExpenseType> LookUpExpenseTypes
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (ExpenseItemViewModel x) => x.LookUpExpenseTypes,
                    getRepositoryFunc: x => x.ExpenseTypes);
            }
        }
    }
}
