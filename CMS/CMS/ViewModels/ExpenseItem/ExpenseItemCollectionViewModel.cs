﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{
    public partial class ExpenseItemCollectionViewModel : CollectionViewModel<ExpenseItem, int, IClinicDataModelUnitOfWork>
    {
        public static ExpenseItemCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExpenseItemCollectionViewModel(unitOfWorkFactory));
        }
        protected ExpenseItemCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.ExpenseItems)
        {
        }
    }
}