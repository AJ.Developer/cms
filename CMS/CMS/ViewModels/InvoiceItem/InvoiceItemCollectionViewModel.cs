﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{
    public partial class InvoiceItemCollectionViewModel : CollectionViewModel<InvoiceItem, int, IClinicDataModelUnitOfWork>
    {
        public static InvoiceItemCollectionViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new InvoiceItemCollectionViewModel(unitOfWorkFactory));
        }
        protected InvoiceItemCollectionViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.InvoiceItems)
        {
        }
    }
}