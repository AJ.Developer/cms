﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CMS.ClinicDataModelDataModel;
using CMS.Common;
using CMS.DataLayer;

namespace CMS.ViewModels
{

    public partial class InvoiceItemViewModel : SingleObjectViewModel<InvoiceItem, int, IClinicDataModelUnitOfWork>
    {

        public static InvoiceItemViewModel Create(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new InvoiceItemViewModel(unitOfWorkFactory));
        }

        protected InvoiceItemViewModel(IUnitOfWorkFactory<IClinicDataModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.InvoiceItems, x => x.InvoiceItemID)
        {
        }
        public IEntitiesViewModel<StockItem> LookUpStockItems
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceItemViewModel x) => x.LookUpStockItems,
                    getRepositoryFunc: x => x.StockItems);
            }
        }
    }
}
